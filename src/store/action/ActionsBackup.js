var addAlert = (text, style) => {
    return {
      type: 'ADD_ALERT',
      text,
      style
    };
  };
  
  var removeAlert = (id) => {
    return {
      type: 'REMOVE_ALERT',
      id
    };
  };
  
   export default {
    addAlert: addAlert,
    removeAlert: removeAlert
  }