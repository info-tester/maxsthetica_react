export const UPDATE_SUCCESS="UPDATE_SUCCESS"
export const UPDATE_ERROR="UPDATE_ERROR"
export const UPDATE_LOADER="UPDATE_LOADER"
export const UPDATE_TOKEN="UPDATE_TOKEN"
export const UPDATE_ID="UPDATE_ID"