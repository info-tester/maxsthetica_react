import { UPDATE_ID, UPDATE_LOADER, UPDATE_SUCCESS, UPDATE_TOKEN } from "../action/Actions";
import { UPDATE_ERROR } from "../action/Actions";

 const initialState={
  success:"",
  error:'',
  loader:false,
  token:'',
  id:''
 }
 const reducer=(state=initialState, action)=>{
   console.log("gygyjv===",action)
   if(action.type===UPDATE_SUCCESS){
      return {...state,success:action.value}
   }
   if(action.type===UPDATE_ERROR){
      return {...state,error:action.value}
   }
   if(action.type===UPDATE_LOADER){
      return {...state,loader:action.value}
   }
   if(action.type===UPDATE_TOKEN){
      return {...state,token:action.value}
   }
   if(action.type===UPDATE_ID){
      return {...state,id:action.value}
   }
  return state;

 }
 export default reducer;