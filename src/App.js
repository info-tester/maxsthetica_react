/** @format */

import React from "react";

import { BrowserRouter as Router, Route } from "react-router-dom";


// screens
import Home from "./components/container/Home";
import Dashboard from "./components/container/Dashboard";
import Login from "./components/auth/Login";
import AboutUs from "./components/cms/AboutUs";
import ContactUs from "./components/cms/ContactUs";
import Faq from "./components/cms/Faq";
// import Privacy_policy from "./components/container/Privacy_policy";
// import TermsandConditions from "./components/container/TermsandConditions";
import RegistrationHealthcareProfessional from "./components/auth/RegistrationHealthcareProfessional";
import RegistrationNonHealthcareProfessional from "./components/auth/RegistrationNonHealthcareProfessional";
import SearchProducts from "./components/container/SearchProducts";
import ProductDetails from "./components/container/ProductDetails";
import ShoppingCart from "./components/container/ShoppingCart";
import Checkout from "./components/container/Checkout";
import AddAddress from "./components/container/AddAddress";
import AddressBook from "./components/container/AddressBook";
import EmailVarification from "./components/auth/EmailVarification";
import { GuestRoute, PrivateRoute } from "./common/private-route";


const App = () => {
	return (
		<Router basename={"/development/maxsthetica_app/"}>
			{/* <Route path="/" component={AddressBook} exact/> */}
			<Route path="/" component={Home} exact/>
			 <GuestRoute path="/registration-healthcare-professional" component={RegistrationHealthcareProfessional} exact/>
			 <GuestRoute path="/registration-non-healthcare-professional" component={RegistrationNonHealthcareProfessional} exact/>
			 <GuestRoute path="/login" component={Login} exact/>
			<PrivateRoute path="/dashboard" component={Dashboard} exact/>
			<Route path="/search-products" component={SearchProducts} exact/>
			<Route path="/product-details" component={ProductDetails} exact/> 
			<Route path="/about-us" component={AboutUs} exact/> 
			<Route path="/contact-us" component={ContactUs} exact/> 
			<Route path="/faq" component={Faq} exact/> 
			<Route path="/cart" component={ShoppingCart} exact/> 
			<Route path="/checkout" component={Checkout} exact/> 
			<PrivateRoute path="/add-address" component={AddAddress} exact/> 
			<PrivateRoute path="/address-book" component={AddressBook} exact/> 
			<Route path="/email-verification/:code" component={EmailVarification}/> 
		</Router>
	);
};

export default App;
