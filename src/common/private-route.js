import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getLSItem } from "./LocalStorage";

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        getLSItem('token') 
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)


export const GuestRoute= ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        getLSItem('token')
        ? <Redirect to={{ pathname: '/dashboard', state: { from: props.location } }} />
        : <Component {...props} />
    )} />
)