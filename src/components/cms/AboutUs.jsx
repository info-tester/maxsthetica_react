import React from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom"
import { useFormik } from 'formik'

import about_us_ban from '../../assets/images/about-us-ban.png';
import about_img from '../../assets/images/about-img.png';
import destination_icon_1 from '../../assets/images/destination-icon-1.png';
import destination_icon_2 from '../../assets/images/destination-icon-2.png';
import vedio_img from '../../assets/images/vedio_img.png';
import palay from '../../assets/images/palay.png';

const AboutUs =(props)=>{
 
  return (
   <Layout>
	   <div className="mar_top"></div>

<div className="about_us_ban pt-0">
  <img src={about_us_ban} alt=""/>
  <div className="about_ban_text">
    <div className="container">
      <div className="abt_ban_text_bx">
        <h2>About Us</h2>
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="#">About Us</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
   
<section className="about-area">
   <div className="container">
      <div className="row align-items-center new_align-roes">
         <div className="col-xl-6 col-lg-5 col-md-5">
            <div className="about-front-img pos-rel">
               <img src={about_img} alt=""/>
            </div>
         </div>

         <div className="col-xl-6 col-lg-7 col-md-7">
            <div className="about-right-side">
            <div className="about-title">
               <h5>About Us</h5>
               <h1>Short Story About Maxsthetica International</h1>
            </div>
            <div className="about-text">
               <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</p>
               <p> Duis aute irure dolor  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</p>
            </div>
            <div className="our-destination">
               <div className="single-item">
                  <div className="mv-icon"><img src={destination_icon_1} alt=""/></div>
                  <div className="mv-title fix">
                     <h3>Our Mission</h3>
                     <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>
                  </div>
               </div>

               <div className="single-item">
                  <div className="mv-icon"><img src={destination_icon_2} alt=""/></div>
                  <div className="mv-title fix">
                     <h3>Our Vission</h3>
                     <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</p>
                  </div>
               </div>
            </div>
            </div>
         </div>

      </div>
   </div>
</section>

<div className="vedio_sec">
   <div className="container">
      <div className="pag_hed">
         <h2>We provide innovative healthcare<br/> solutions for our clients</h2>
      </div>
      <div className="vedio_inr">
         <div className="videoBox">
            <img src={vedio_img} alt=""/>
            <div className="playIcon">
               <a href="http://www.youtube.com/watch?v=opj24KnzrWo" className="fancybox-media">
                  <img src={palay} alt="" rel="media-gallery"/></a>
            </div>
         </div>
      </div>
   </div>
</div>
</Layout>

  );
}

export default AboutUs;
