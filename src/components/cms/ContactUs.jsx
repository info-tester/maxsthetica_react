import React from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';

import about_us_ban from '../../assets/images/about-us-ban.png';

import hou from '../../assets/images/hou.png';
import ema from '../../assets/images/ema.png';
import call from '../../assets/images/call.png';
import what from '../../assets/images/what.png';


const ContactUs =(props)=>{
 
  return (
   <Layout>
   <div className="mar_top"></div>
      <div className="about_us_ban pt-0">
         <img src={about_us_ban} alt=""/>
         <div className="about_ban_text">
            <div className="container">
               <div className="abt_ban_text_bx">
                  <h2>Contact Us</h2>
                  <ul>
                     <li><a href="index.html">Home</a></li>
                     <li><a href="#">Contact Us</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <section className="about-area">
         <div className="container">
            <div className="row">
               <div className="col-12">
                  <div className="title_headings">
                     <div className="pag_hed">
                        <h3>Get In Touch</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
                     </div>
                  </div>
               </div>
               <div className="col-md-4 order-8 col-sm-12">
                 <div className="contact-from">
                   <div className="cont-box">
                                 <img src={hou}/>
                                 <div> 
                                     <h3>Reach Us On</h3>
                                     <p>Ground Floor, 2C South Stage, Michigan Avenue, Salford Quays, M50 2GY</p>
                                 </div>
                             </div>
                             <div className="cont-box">
                                 <img src={ema}/>
                                 <div> 
                                     <h3>Email Us On</h3>
                                     <p><a href="mailto:manager@maxsthetica. co.uk">manager@maxsthetica. co.uk</a> </p>
                                 </div>
                             </div>
                             <div className="cont-box">
                                 <img src={call}/>
                                 <div> 
                                     <h3>Call Us On</h3>
                                     <p><a href="tel:0161 848 7146 Option 7">0161 848 7146 Option 7</a></p>
                                 </div>
                             </div>
                              <div className="cont-box">
                                 <img src={what}/>
                                 <div> 
                                     <h3>WhatsApp On</h3>
                                     <p><a href="https://wa.me/07879 893 781" target="_blank">07879 893 781</a></p>
                                 </div>
                             </div>
                 </div>
               </div>
               <div className="col-md-8  col-sm-12">
                  <div className="contact-from">
                     <div className="row">
                        <div className="col-sm-6">
                           <div className="contact_inputs">
                                 <label>Name *</label>
                                 <div className="inp_rea"><i className="fa fa-user"></i><input type="text" placeholder="Enter here"/></div>
                            </div>
                        </div>

                        <div className="col-sm-6">
                           <div className="contact_inputs">
                                 <label>Email *</label>
                                <div className="inp_rea"><i className="fa fa-envelope"></i><input type="email" placeholder="Enter here"/></div>
                            </div>
                        </div>

                        <div className="col-sm-12">
                           <div className="contact_inputs">
                                 <label>Subject *</label>
                                <div className="inp_rea"><i className="fa fa-user"></i><input type="text" placeholder="Enter here"/></div>
                            </div>
                        </div>
                        
                        <div className="col-sm-12">
                           <div className="contact_inputs">
                                 <label>Message *</label>
                                <div className="inp_rea text-i"><i className="fa fa-text-height"></i><textarea rows="6" placeholder="Type Your Message *"></textarea></div>
                            </div>
                        </div>
                       
                        <div className="col-sm-12">
                           <button type="submit" className="pg_btn ne_pg_btns">Submit Now</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div className="vedio_sec">
         <div className="container">
            <div className="row">
               <div className="col-12">
                  <div className="iframe_divs">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4749.239448332931!2d-2.293949!3d53.475256!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x163626880a037b8e!2sMaxsthetica%20International!5e0!3m2!1sen!2suk!4v1645431099745!5m2!1sen!2suk" width="100%" height="450" style={{border:0}} allowfullscreen="" loading="lazy"></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
</Layout>

  );
}

export default ContactUs;
