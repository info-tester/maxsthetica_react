import React from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';

import about_us_ban from '../../assets/images/about-us-ban.png';


const Faq =(props)=>{
 
  return (
   <Layout>
   <div className="mar_top"></div>
      <div className="about_us_ban pt-0">
         <img src={about_us_ban} alt=""/>
         <div className="about_ban_text">
            <div className="container">
               <div className="abt_ban_text_bx">
                  <h2>Faq</h2>
                  <ul>
                     <li><a href="index.html">Home</a></li>
                     <li><a href="#">Faq</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <section className="about-area">
         <div className="container">
            <div className="">
               <div className="pag_hed">
         <h4>Frequently Asked Questions</h4>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
      </div>
      <div className="faq_inr">
         <div className="accordian-faq">
            <div className="accordion" id="faq">
               <div className="card">
                  <div className="card-header" id="faqhead1">
                     <a href="#" className="collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">
                        Q. Can you supply products directly to aesthetics clinics?
                     </a>
                  </div>
                  <div id="faq1" className="collapse" aria-labelledby="faqhead1" data-parent="#faq">
                     <div className="card-body">
                        <p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
                     </div>
                  </div>
               </div>
               <div className="card">
                  <div className="card-header" id="faqhead2">
                     <a href="#" className="collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">
                        Q. Are you just an aesthetics pharmacy?
                     </a>
                  </div>
                  <div id="faq2" className="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                     <div className="card-body">
                        <p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
                     </div>
                  </div>
               </div>
               <div className="card">
                  <div className="card-header" id="faqhead3">
                     <a href="#" className="collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">
                        Q.  Can I order my prescription medication from you?
                     </a>
                  </div>
                  <div id="faq3" className="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                     <div className="card-body">
                        <p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
                     </div>
                  </div>
               </div>
               <div className="card">
                  <div className="card-header" id="faqhead4">
                     <a href="#" className="collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">
                        Q. Can you supply products directly to aesthetics clinics?
                     </a>
                  </div>
                  <div id="faq4" className="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                     <div className="card-body">
                        <p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
                     </div>
                  </div>
               </div>
               <div className="card">
                  <div className="card-header" id="faqhead5">
                     <a href="#" className="collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5">
                        Q. Are you just an aesthetics pharmacy?
                     </a>
                  </div>
                  <div id="faq5" className="collapse" aria-labelledby="faqhead5" data-parent="#faq">
                     <div className="card-body">
                        <p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
                     </div>
                  </div>
               </div>
               <div className="card">
                  <div className="card-header" id="faqhead6">
                     <a href="#" className="collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6">
                        Q.  Can I order my prescription medication from you?
                     </a>
                  </div>
                  <div id="faq6" className="collapse" aria-labelledby="faqhead6" data-parent="#faq">
                     <div className="card-body">
                        <p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
            </div>
         </div>
      </section>
      
</Layout>

  );
}

export default Faq;
