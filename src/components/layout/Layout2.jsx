import React, { Component, Fragment } from "react";
import Header from "./Header2";
import Footer from "./Footer";
import Loader from "./Loader";

class Layout extends Component {
  render() {
    return (
      <Fragment>

        <Header />
        <Loader />

        {this.props.children}
        <Footer />
      </Fragment>
    );
  }
}
export default Layout;
