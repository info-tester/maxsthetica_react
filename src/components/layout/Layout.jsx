import React, { Component, Fragment } from "react";
import Header from "./Header";
import Header2 from "./Header2";
import Footer from "./Footer";
import Loader from "./Loader";

class Layout extends Component {

  constructor(props, context) {
		super(props, context)
		this.state = {
			user_data:{},
      token:''
   
		}
	}

  componentDidMount(){
     var data= localStorage.getItem("token");
     this.setState({token : data})

  };


  render() {
    return (
      <Fragment>
        <Loader />

     {this.state.token ? <Header2 />:<Header/>}   
  {this.props.children}
        <Footer />
      </Fragment>
    );
  }
}
export default Layout;
