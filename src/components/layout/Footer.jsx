import React, { Component } from "react";
import { Link } from "react-router-dom";

import logo1 from "../../assets/images/logo1.png";
import get_in1 from "../../assets/images/get_in1.png";
import get_in2 from "../../assets/images/get_in2.png";
import get_in3 from "../../assets/images/get_in3.png";


class Footer extends Component {
     
render() { 
return (
<>

<footer className="footer_sec">
	<div className="footer_top">
		<div className="container">
			<div className="footer_top_inr">
				<div className="footer_top_left">
					<img src={logo1} alt=""/>
				</div>
				<div className="footer_top_right">
					<span>Follow us</span>
					<ul>
						<li><a href="#" translate="_blank"><i className="fab fa-facebook-f"></i></a></li>
						<li><a href="#" translate="_blank"><i className="fab fa-twitter"></i></a></li>
						<li><a href="#" translate="_blank"><i className="fab fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div className="footer_body">
		<div className="container">
			<div className="footer_body_inr">
				<div className="row">
					<div className="col-sm-3">
						<div className="footer_bx">
							<h4>About Us</h4>
							<p>Lorem Ipsum is simply dummy text of the  and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type  scrambled it to make a type specimen book  has not</p>
							<a href="#" className="rede_more">+ Read more</a>
						</div>
					</div>
					<div className="col-sm-3">
						<div className="footer_bx">
							<h4>Quick Links</h4>
							<ul>
								<li><Link to="/">Home</Link></li>
								<li><Link to="/about-us">About us</Link></li>
								<li><Link to="/login">Log in</Link></li>
								<li><a href="#">Register</a></li>
								<li><Link to="/search-products">Shop Now</Link></li>
							</ul>
						</div>
					</div>
					<div className="col-sm-3">
						<div className="footer_bx">
							<h4></h4>
							<ul>
								<li><a href="#">Treatments</a></li>
								<li><a href="#">Aesthetics</a></li>
								<li><a href="#">News</a></li>
								<li><Link to="/contact-us">Contact</Link></li>
								<li><Link to="/faq">FAQ</Link></li>
							</ul>
						</div>
					</div>
					<div className="col-sm-3">
						<div className="footer_bx">
							<h4>Get in touch</h4>
							<ul>
								<li><em><img src={get_in1} alt=""/></em> Ground Floor, 2C South Stage, Michigan Avenue, Salford Quays, M50 2GY </li>
								<li><em><img src={get_in2} alt=""/></em>
									<a href="tel:01618487146">0161 848 7146</a> Option 7  / 
									<a href="tel:07879893781">07879 893 781</a>
								</li>
								<li><em><img src={get_in3} alt=""/></em><a href="mailto:manager@maxsthetica.co.uk">manager@maxsthetica. co.uk </a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div className="footer_bot">
		<div className="container">
			<div className="footer_bot_inr">
				<div className="footer_bot_left">
					<ul>
						<li>Copyright © 2022  <a href="index.html">www.maxsthetica.co.uk</a> </li>
						<li>All Rights Reserved.</li>
					</ul>
				</div>
				<div className="footer_bot_rig">
					<ul>
						<li><a href="#url">Terms and conditions</a></li>
						<li><a href="#url">Privacy policy </a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
</>
  );
}
}
export default Footer;
