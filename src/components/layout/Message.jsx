import React , {Component}from 'react'

import { connect } from 'react-redux'
import {UPDATE_SUCCESS,UPDATE_ERROR} from "../../store/action/Actions"

class Messege extends Component {

  componentDidMount(){
    setTimeout(() => { this.props.updateError("") ; 
    this.props.updateSuccess("") 
  }, 5000);

  }
  
  render(){

  return (
      <>
      {this.props.success?
        <div className="alert alert-success alert-dismissible">
        <a href="#" className="close" onClick={()=> this.props.updateSuccess("")} >&times;</a>
            <strong>
                {this.props.success}
            </strong>
        </div>
        :null} 
       {this.props.error ?  <div className="alert alert-danger alert-dismissible">
        <a href="#" className="close" onClick={()=> this.props.updateError("")}>&times;</a>
        <strong>
            {this.props.error}
        </strong>
    </div>:null}
    </>
  )
}
}
const mapStateToProps=state=>{
	return {
		success:state.success,
		error:state.error
	}
}

var mapDispatchToProps = (dispatch) => {
  return {
   updateSuccess: (msg)=> dispatch({type: UPDATE_SUCCESS, value: msg})  ,   
   updateError: (msg)=> dispatch({type: UPDATE_ERROR, value: msg})     

  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Messege)