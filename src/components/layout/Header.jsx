import React, { Component } from "react";
import { Link } from "react-router-dom";

import logo from "../../assets/images/logo.png";
import login from "../../assets/images/login.png";
import login1 from "../../assets/images/login1.png";
import drop from "../../assets/images/drop.png";

class Header extends Component {
  render() {
    return (
      <>
   <header className="header_sec header_inr_sec">
	<div className="container">
		<nav className="navbar navbar-expand-lg nav_top">
			<Link className="navbar-brand" to="/">
				<img src={logo} alt="logo" />
			</Link>
			  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span className="icon-bar"></span>
				<span className="icon-bar"></span>
				<span className="icon-bar"></span>
			  </button>
			  <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
			  	{/* <!-- <ul className="mr-auto"> */}
			  	{/* </ul> --> */}
		        <ul className="navbar-nav menu_sec">
		             <li><Link to="/">Home</Link></li>
		             <li><Link to="/search-products">Shop Now</Link></li>
		             <li><a href="#">Treatments</a></li>
		             <li><a href="#">Aesthetics</a></li>
		             <li><a href="#">News</a></li>
		             <li><a href="#">Contact</a></li>
					 <li><a href="#" className="reg_ph">Registration Healthcare professional</a></li>
                   <li><a href="#" className="reg_ph">Registration non Healthcare Professional</a></li>
		        </ul>
			  </div>
			  <div className="logon_btn_div">
			  	<ul>
			  		<li><Link to="/login" className="login_btn">
			  			<img src={login} alt="" className="login_img"/>
			  			<img src={login1} alt="" className="login_img1"/>Login</Link>
			  		</li>
			  		
					  <li className="reg_dex">
						<a className="sign_btn" id="sign_btns">Register <img src={drop} alt=""/></a>
						<div className="sign_btn_bx" id="sign_btn_bx">
							<ul>
							<li><Link to="/registration-healthcare-professional"> Registration Healthcare professional</Link></li>
							<li><Link to="/registration-non-healthcare-professional">Registration non Healthcare Professional</Link></li>
							</ul>
						</div>
					</li>
			  	</ul>
			  </div>
		</nav>
	</div>
</header>
      </>
    );
  }
}
export default Header;
