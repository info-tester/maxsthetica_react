import React from 'react'
import {store} from "../../index.js"
import { useStore  } from 'react-redux'

const Messege = () => {
  
  const state = useStore().getState()
  console.log("kp-",state);

  return (
      <>
      {state.success?
        <div className="alert alert-success alert-dismissible">
        <a href="#" className="close" >&times;</a>
            <strong>
                {state.success}
            </strong>
        </div>
        :null} 
       {state.error ?  <div className="alert alert-danger alert-dismissible">
        <a href="#" className="close">&times;</a>
        <strong>
            {state.error}
        </strong>
    </div>:null}
    </>
  )
}

export default Messege