import React, { Component } from "react";
import { Link } from "react-router-dom";

import logo from "../../assets/images/logo.png";
import pro_pick from "../../assets/images/pro_pick.png";
import drop_iput from "../../assets/images/drop_iput.png";

class Header extends Component {
  constructor(props, context) {
		super(props, context)
		this.state = {
			user_data:{},
         name:''
   
		}
	}

  componentDidMount(){
     var data= localStorage.getItem("name");
     this.setState({name:data})

  };
  render() {
    return (
      <header className="header_sec header_inr_sec aft_login">
     <div className="container">
        <nav className="navbar navbar-expand-lg nav_top">
           <Link className="navbar-brand" to="/">
           <img src={logo} alt="logo" />
           </Link>
           <div className="log_hed_pro">
            <a href="#url" id="profidrop" className="hed_img_tog">
              <img src={pro_pick} alt=""/>
              <span>Hi, {this.state.name}<i><img src={drop_iput}/></i></span>
            </a>
            <div className="profidropdid" id="profidropdid">
                <ul>
                 <li><Link to="/dashboard">Dashboard</Link></li>
                 <li><a href="">Order History</a></li>
                 <li><Link to="/address-book">Address Book</Link></li>
                 <li><a href="">Payment History</a></li>
                 <li><a href="">My Favorites</a></li>
                 <li><Link to="/"
                  onClick={()=> localStorage.setItem("token","")}>Log Out</Link></li>

                </ul>
              </div>
            </div>
        </nav>
     </div>
  </header>
    );
  }
}
export default Header;
