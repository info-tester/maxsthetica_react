import React, {Component, useState } from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';
import {ErrorMessage, Formik, Form, Field } from 'formik';
import FormikErrorFocus from 'formik-error-focus'
import { connect } from "react-redux";
import Message from '../layout/Message';
import axios from "../../common/axios"
import {UPDATE_SUCCESS, UPDATE_ERROR,UPDATE_LOADER} from "../../store/action/Actions"
import * as Yup from 'yup';

import uploadnews from '../../assets/images/uploadnews.png'
import uploadnews1 from '../../assets/images/uploadnews1.png'


const phoneRegExp = /^[0-9\b]+$/

const SignupSchema = Yup.object().shape({

   username: Yup.string()
     .required('Please enter name'),
   email: Yup.string()
   .required('Please enter email ')
   .email("Please enter a valid email"),
   pass: Yup.string()
     .min(8, 'Password must be more than or equals to 8 characters')
     .required('Please enter password'),
   con_pass: Yup.string()
   .required('Please enter confirm password')
   .oneOf([Yup.ref('pass')], 'Your passwords do not match.'),

   fname: Yup.string()
   .required('Please enter first name'),
   lname: Yup.string()
   .required('Please enter last name'),
   contact_phone: Yup.string().matches(phoneRegExp, 'Phone number is not valid')
   .required('Please enter contact number')
   .min(10,'Contact number must be 10 digits')
   .max(10,'Contact number must be 10 digits'),
   street_address: Yup.string()
   .required('Please enter street address'),
   city: Yup.string()
   .required('Please enter town/city'),
   postcode: Yup.string()
   .required('Please enter postcode'),
   pr: Yup.string()
   .required('Please enter'),
   r_number: Yup.string()
   .required('Please enter registration number'),
   list_courses: Yup.string()
   .required('Please enter courses'),
   file11: Yup
   .mixed()
   .required("Please upload certificates and insurance"),
   file22: Yup
   .mixed()
   .required("Please upload proof of Id"),
   checkbox: Yup.boolean()
   .oneOf([true], "You must accept the practitioner confirmation") 
});
    
 class RegistrationHealthcareProfessional extends Component {
   constructor(props, context) {
		super(props, context)
		this.state = {
         loader:false,
         result_loader:"",
         resStatus:''
		}
	}

   scrollToTop = () => {
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	};
   render() { 
  return (
   <Layout>
     <div className="mar_top"></div>

{this.state.result_loader==""?
     <div className="main_wrapper">
        {/* {this.state.loader ?
   <div class="loader_img_new">
	

	<div class="container">
    <div class="row">
    	<div class="loader_divs">
        <div class="loader5"></div>
        </div>
      </div>
    </div>
</div>:null} */}
         <section className="login_sec">
            <div className="container">
               <div className="row">
                  <div className="col-12">
                     <div className="main-center-div registration_div">
                        <div className="upper-login">
                        <Message/>

                        	<div className="login_heading">
	                           <h2>Registration – Healthcare Professional</h2>
	                           <p>Please fill in the below fields to create an account</p>
	                        </div>
                           <Formik
                              initialValues={{
                                 username: '',
                                 email: '',
                                 pass: '',
                                 con_pass: '',

                                 fname:'',
                                 lname:'',
                                 bname:'',
                                 vn:'',
                                 crn:'',
                                 contact_phone:'',

                                 street_address:'',
                                 address2:'',
                                 city:'',
                                 postcode:'',

                                 pr:'',
                                 r_number:'',
                                 list_courses:'',

                                 checkbox: false,

                                 file1:[],
                                 file11:null,
                                 file2:[],
                                 file22:null,

                              }}
                              validationSchema={SignupSchema}
                              onSubmit={values => {
                                 // same shape as initial values
                                 console.log("==========",values.file2);
                                 console.log("==========",values.file1);

                                 // this.setState({loader: true})
                                 this.props.updateLoader(true)

                                 var formData= new FormData();
                                 formData.append("username",values.username)
                                 formData.append("email",values.email)
                                 formData.append("password",values.pass)
                                 formData.append("confirm_password",values.con_pass)
                                 formData.append("first_name",values.fname)
                                 formData.append("last_name",values.lname)
                                 formData.append("buisness_name",values.bname)
                                 formData.append("vat_number",values.vn)
                                 formData.append("company_registration_number",values.crn)
                                 formData.append("contact_number",values.contact_phone)
                                 formData.append("street_address",values.street_address)
                                 formData.append("address2",values.address2)
                                 formData.append("city",values.city)
                                 formData.append("postcode",values.postcode)
                                 formData.append("professional_regulator",values.pr)
                                 formData.append("registration_no",values.r_number)

                                 formData.append("courses",values.list_courses)
                                
                                 // formData.append("doc_type",'P')
                                 values.file1.map((item, index) => {
                                    formData.append("certificate[]",item)

                                 })
                                 values.file2.map((item, index) => {
                                 formData.append("proof[]",item)
                                 })

                                 // axios({
                                 //    method: "post",
                                 //    url: "https://phpwebdevelopmentservices.com/development/maxsthetica_code/api/registrationHealthCare",
                                 //    data: formData,
                                 //  })
                                  axios.post("registrationHealthCare",formData)

                                  .then((response) => {
                                    console.log(response.data.error)
                                    // this.setState({loader:false})
                                    this.props.updateLoader(false)

                                    if(response.data.result){
                                       this.setState({result_loader:"true", resStatus: response.data.result.status})

                                    // this.props.history.push("/login")
                        
                                    }else{
                                       if(response.data.error.validation.username){
                                       this.props.updateError(response.data.error.validation.username[0])

                                    }
                                       if(response.data.error.validation.contact_number){
                                       this.props.updateError(response.data.error.validation.contact_number[0])

                                    }
                                       if(response.data.error.validation.email){
                                       this.props.updateError(response.data.error.validation.email[0])

                                    }
                                    this.scrollToTop()
                                 }
                                 setTimeout(() => { this.props.updateError(""); this.props.updateSuccess("")
                              }, 5000);                                   
                                 })
                               
                              }}
                           >
                              {({ errors, touched, values, setFieldValue }) => (
                                 <Form>
                                     <div className="input-from">
                                <div className="row">
                                   <div className="col-12">
                                      <h4 className="registr_head mt-0">
                                         Registration details
                                      </h4>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="username">Username * </label>
                                          <Field type="text" name="username" placeholder="Enter Username" />
                                          <span className="error">
                                      <ErrorMessage name="username"/>          
                                  </span>  
                                     
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">User Email * </label>
                                          <Field type="text" name="email" placeholder="Enter email address" />
                                          <span className="error">
                                      <ErrorMessage name="email"/>          
                                  </span>  
                                      
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">User Password *</label>
                                          <div className="password-in">
                                             <Field id="password-field"  type="password" name="pass"   placeholder="Enter password"/>
                                          </div>
                                          <span className="error">
                                      <ErrorMessage name="pass"/>          
                                  </span>  
                                       </div>
                                   </div>

                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Confirm Password *</label>
                                          <div className="password-in">
                                             <Field id="password-field"   type="password" name="con_pass"   placeholder="Enter confirm password"/>
                                             
                                          </div>
                                          <span className="error">
                                      <ErrorMessage name="con_pass"/>          
                                  </span>  
                                       </div>
                                   </div>
                                </div>

                                <div className="row">
                                   <div className="col-12">
                                      <h4 className="registr_head">
                                         Application details
                                      </h4>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">First Name * </label>
                                          <Field type="text" name="fname" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="fname"/>          
                                  </span>  
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Last Name * </label>
                                          <Field type="text" name="lname" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="lname"/>          
                                  </span>  
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Business Name (If Applicable)</label>
                                          <Field type="text" name="bname" placeholder="Enter business name "/>
                                     
                                       </div>
                                   </div>

                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Vat Number (If Applicable)</label>
                                          <Field type="text" name="vn" placeholder="Enter here"/>
                                       </div>
                                   </div>

                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Company Registration No (If Applicable)</label>
                                          <Field type="text" name="crn" placeholder="Enter here"/>
                                       </div>
                                   </div>

                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Contact Number *</label>
                                          <Field type="text" name="contact_phone" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="contact_phone"/>          
                                  </span>  
                                       </div>
                                   </div>
                                </div>

                                <div className="row">
                                   <div className="col-12">
                                      <h4 className="registr_head">
                                         Customer address
                                      </h4>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Street Address *</label>
                                          <Field type="text" name="street_address" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="street_address"/>          
                                  </span>  
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Address Line 2 </label>
                                          <Field type="text" name="address2" placeholder="Enter here"/>
                                     
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Town/City *</label>
                                          <Field type="text" name="city" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="city"/>          
                                  </span>  
                                       </div>
                                   </div>

                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Postcode *</label>
                                          <Field type="text" name="postcode" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="postcode"/>          
                                  </span>  
                                       </div>
                                   </div>
                                </div>

                                <div className="row">
                                   <div className="col-12">
                                      <h4 className="registr_head">
                                         Professional Details

                                      </h4>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Professional Regulator(GPMC,GPHc,NMC)*</label>
                                          <Field type="text" name="pr" placeholder="Enter here" />
                                          <span className="error">
                                      <ErrorMessage name="pr"/>          
                                  </span>  
                                      </div>
                                   </div>
                                   <div className="col-lg-4 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Registration No * </label>
                                          <Field type="text" name="r_number" placeholder="Enter here" /> 
                                          <span className="error">
                                      <ErrorMessage name="r_number"/>          
                                  </span>  
                                      </div>
                                   </div>

                                   <div className="col-lg-12">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">List Courses/Training Qualified In *</label>
                                          <Field as="textarea" placeholder="Enter here" name="list_courses" />

                                          <span className="error">
                                      <ErrorMessage name="list_courses"/>          
                                  </span>  
                                       </div>
                                   </div>
                                </div>

                                


                                <div className="row">
                                   <div className="col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Upload Certificates and Insurance *</label>
                                          <div className="uplodimgfilnew">
                                             <input type="file" id="file1" className="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple
                                            onChange={(event)=>  {if(values.file1.length<10){for(let i=0;i<event.target.files.length;i++){values.file1.push(event.target.files[i])};
                                            setFieldValue("file11",values.file1)}}} />
                                             <label htmlFor="file1">
                                             <img src={uploadnews} alt="" className="hoven"/>
                                                <img src={uploadnews1} alt="" className="hoveb"/>
                                                <p>Drop your file here or click here to upload </p>
                                                <span>You can upload up to 10 files</span>
                                             </label>
                                          </div>
                                          {values.file11 ? 
                                          values.file11.map((item,index)=>(
                                             <div style={{flexDirection:'column',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                             <div style={{height:10,width:10,borderRadius:5,backgroundColor:'#f673c1',marginBottom:-20,marginRight:10,alignSelf:'center'}}/>
                                             <div style={{alignSelf:'center',marginLeft:20}}>{item.name}</div>
                                             </div>                                          ))
                                          : null}

                                          <span className="error">
                                      <ErrorMessage name="file11"/>          
                                  </span>
                                  </div>
                                    
                                   </div>
                                   
                                   <div className="col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Proof of Id * </label>
                                          <div className="uplodimgfilnew">
                                             <input type="file" id="file2" className="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple
                                                onChange={(event)=> {if(values.file2.length<10){for(let i=0;i<event.target.files.length;i++){values.file2.push(event.target.files[i])};
                                                setFieldValue("file22",values.file2)}}} />
                                               <label htmlFor="file2">
                                               <img src={uploadnews} alt="" className="hoven"/>
                                                <img src={uploadnews1} alt="" className="hoveb"/>
                                                <p>Drop your file here or click here to upload </p>
                                                <span>You can upload up to 10 files</span>
                                             </label>
                                          </div>
                                          {values.file22 ? 
                                          values.file22.map((item, index) => ( 
                                             <div style={{flexDirection:'column',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                             <div style={{height:10,width:10,borderRadius:5,backgroundColor:'#f673c1',marginBottom:-20,marginRight:10,alignSelf:'center'}}/>
                                             <div style={{alignSelf:'center',marginLeft:20}}>{item.name}</div>
                                             </div>
                                          )) 
                                          : null}

                                          <span className="error">
                                      <ErrorMessage name="file22"/>          
                                  </span>
                                  </div>
                                   </div>
                               

                                </div>
                               

                                <div className="row">
                                    <div className="col-12">
                                      <h4 className="registr_head">
                                         Practitioner Confirmation
                                      </h4>
                                    </div>
                                    <div className="col-md-12 col-sm-12">
                                      <div className="input_fields">
                                          <h2>I am a registered Practitioner *</h2>
                                          <label className="list_checkBox" >
                                             <p>  I, the undersigned, am a registered Practitioner. I will personally undertake the ordering of any medicinal product using the assigned Maxsthetica International protocol. I, the practitioner acting on behalf of the clinic or customer, am prepared to undertake the responsibility to ensure that the product is received, stored and used as stated in the legislation. I agree to the company Terms & Conditions and Privacy Statement. </p>
                                             <Field type="checkbox" name="checkbox" />
                                             <span className="list_checkmark new_achec"></span>
                                          </label>
                                      </div>
                                   </div>
                                </div>
                                <span className="error">
                                      <ErrorMessage name="checkbox"/>          
                                  </span>   

                                <div className="row">
                                 <div className="col-12">
                                    <div className="border-lins"></div>
                                 </div>
                                </div>

                                <div className="row">
                                 <div className="col-12">
                                    <div className="text-center">
                                    <button type="submit" className="subm_btns">Submit</button>
                                    </div>
                                 </div>
                                </div>

                              </div>

                              <FormikErrorFocus
            // See scroll-to-element for configuration options: https://www.npmjs.com/package/scroll-to-element
            offset={0}
            align={'middle'}
            focusDelay={200}
            ease={'linear'}
            duration={1000}
          />

                              </Form>
       )}
     </Formik>
                             </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>


      </div>
      :
      <div class="loader_img">
      <div class="container">
       <div class="row">
          <div class="loader_divs">
              <div class="scs_msg">
                 <img src="images/success.png"/>
                 <h2>Success!</h2>
                 <p>{this.state.resStatus} </p>
                 <Link to="/login">Go to Login</Link>
              </div>
           </div>
         </div>
       </div>
   </div>  
        
}

</Layout>
  );
}
 }
 var mapDispatchToProps = (dispatch) => {
   return {
    updateSuccess: (msg)=> dispatch({type: UPDATE_SUCCESS, value: msg})  ,   
    updateError: (msg)=> dispatch({type: UPDATE_ERROR, value: msg}) ,    
    updateLoader: (msg)=> dispatch({type: UPDATE_LOADER, value: msg}) ,    

   }
 }
 
export default connect(null,mapDispatchToProps)(RegistrationHealthcareProfessional);