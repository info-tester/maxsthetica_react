import React, { Component, useState } from "react";
import Layout from "../layout/Layout";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import { ErrorMessage, Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormikErrorFocus from "formik-error-focus";
import { connect } from "react-redux";
import Message from '../layout/Message';
import axios from "../../common/axios"
import {UPDATE_SUCCESS, UPDATE_ERROR,UPDATE_LOADER} from "../../store/action/Actions"

import uploadnews from "../../assets/images/uploadnews.png";
import uploadnews1 from "../../assets/images/uploadnews1.png";

const phoneRegExp = /^[0-9\b]+$/;

const SignupSchema = Yup.object().shape({
  username: Yup.string()
  .required("Please enter name"),
  email: Yup.string().required("Please enter email ").email("Please enter a valid email"),
  pass: Yup.string()
    .min(8, "Password must be more than or equals to 8 characters")
    .required("Please enter password"),
  con_pass: Yup.string()
    .required("Please enter confirm password")
    .oneOf([Yup.ref("pass")], "Your passwords do not match."),

  fname: Yup.string().required("Please enter first name"),
  lname: Yup.string().required("Please enter last name"),
  contact_phone: Yup.string()
    .matches(phoneRegExp, "Phone number is not valid")
    .required("Please enter contact number")
    .min(10, "Contact number must be 10 digits")
    .max(10, "Contact number must be 10 digits"),
  street_address: Yup.string().required("Please enter street address"),
  city: Yup.string().required("Please enter town/city"),
  postcode: Yup.string().required("Please enter postcode"),
  // pr: Yup.string().required("Please enter"),
  // r_number: Yup.string().required("Please enter registration number"),
  courses: Yup.string().required("Please enter list training & qualifications"),
  file11: Yup
  .mixed()
  .required("Please upload certificates and insurance"),
  file22: Yup
  .mixed()
  .required("Please upload proof of Id"),
  checkbox: Yup.boolean().oneOf(
    [true],
    "You must accept the practitioner confirmation"
  ),
  checkbox2: Yup.boolean().oneOf(
    [true],
    "You must accept the clinical oversight"
  ),
  oversight_id2: Yup.string().required("Please select at least one clinical oversight"),
 
  yes: Yup.string().required("Please select"),
 
});

class RegistrationNonHealthcareProfessional extends Component {
  constructor(props, context) {
		super(props, context)
		this.state = {
			oversite_data:[],
      loader:false,
      result_loader:"",
      resStatus:''
		}
	}

  componentDidMount(){

    // axios.get('https://phpwebdevelopmentservices.com/development/maxsthetica_code/api/get-clinical-oversight')
    axios.get("get-clinical-oversight")

    .then(resp => {

    this.setState({oversite_data: resp.data.details});
});
    
  }


  scrollToTop = () => {
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	};

  render() {
    return (
      <Layout>
        <div className="mar_top"></div>
        {this.state.result_loader==""?
     <div className="main_wrapper">
        {/* {this.state.loader ?
   <div className="loader_img_new">
	

	<div className="container">
    <div className="row">
    	<div className="loader_divs">
        <div className="loader4"></div>
        </div>
      </div>
    </div>
</div>:null} */}
          <section className="login_sec">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <div className="main-center-div registration_div">
                    <div className="upper-login">
                    <Message/>

                      <div className="login_heading">
                        <h2>Registration – Non-Healthcare Professional</h2>
                        <p>
                          Please fill in the below fields to create an account
                        </p>
                      </div>
                      <Formik
                        initialValues={{
                          username: "",
                          email: "",
                          pass: "",
                          con_pass: "",

                          fname: "",
                          lname: "",
                          bname: "",
                          vn: "",
                          crn: "",
                          contact_phone: "",

                          street_address: "",
                          address2: "",
                          city: "",
                          postcode: "",

                          courses: "",
                          co_provider: "",

                          checkbox: false,
                          checkbox2: false,

                          file1: [],
                          file2: [],
                          file11: null,
                          file22: null,

                          oversight_id: [],
                          oversight_id2: '',
                        
                          yes: "",
                        }}
                        validationSchema={SignupSchema}
                        onSubmit={(values) => {
                          // same shape as initial values
                          console.log("===", values);
                          // console.log("==========", values.oversight_id2);

                          // this.setState({loader: true})
                          this.props.updateLoader(true)

                          var formData= new FormData();
                          formData.append("username",values.username)
                          formData.append("email",values.email)
                          formData.append("password",values.pass)
                          formData.append("confirm_password",values.con_pass)
                          formData.append("first_name",values.fname)
                          formData.append("last_name",values.lname)
                          formData.append("buisness_name",values.bname)
                          formData.append("vat_number",values.vn)
                          formData.append("company_registration_number",values.crn)
                          formData.append("contact_number",values.contact_phone)
                          formData.append("street_address",values.street_address)
                          formData.append("address2",values.address2)
                          formData.append("city",values.city)
                          formData.append("postcode",values.postcode)
                          

                          formData.append("courses",values.courses)

                          values.oversight_id.map((item,index)=>{
                            formData.append("oversight_id[]", item)
                          })

                          formData.append("ACPB",values.yes)
                          formData.append("co_provider_details",values.co_provider)
                         
                          // formData.append("doc_type",'P')
                          values.file1.map((item, index) => {
                             formData.append("certificate[]",item)

                          })
                          values.file2.map((item, index) => {
                          formData.append("proof[]",item)
                          })

                          // axios({
                          //    method: "post",
                          //    url: "https://phpwebdevelopmentservices.com/development/maxsthetica_code/api/registrationNonHealthCare",
                          //    data: formData,
                          //  })
                           axios.post("registrationNonHealthCare",formData)

                           .then((response) => {
                             console.log(response.data.error)
                            //  this.setState({loader:false})
                            this.props.updateLoader(false)

                             if(response.data.result){
                              this.setState({result_loader:"true", resStatus: response.data.result.status})
                              // props.history.push("/reg3")
                 
                            }else{
                              if(response.data.error.validation.username){
                              this.props.updateError(response.data.error.validation.username[0])

                           }
                              if(response.data.error.validation.contact_number){
                              this.props.updateError(response.data.error.validation.contact_number[0])

                           }
                              if(response.data.error.validation.email){
                              this.props.updateError(response.data.error.validation.email[0])

                           }
                           this.scrollToTop()
                        }
                        setTimeout(() => { this.props.updateError(""); this.props.updateSuccess("")
                      }, 5000);                             
                          })
                         

                        }}
                      >
                        {({ errors, touched, values, setFieldValue }) => (
                          <Form>
                            <div className="input-from">
                              <div className="row">
                                <div className="col-12">
                                  <h4 className="registr_head mt-0">
                                    Registration details
                                  </h4>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="username">Username * </label>
                                    <Field
                                      type="text"
                                      name="username"
                                      placeholder="Enter Username"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="username" />
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">User Email * </label>
                                    <Field
                                      type="text"
                                      name="email"
                                      placeholder="Enter email address"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="email" />
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      User Password *
                                    </label>
                                    <div className="password-in">
                                      <Field
                                        id="password-field"
                                        autoComplete="off"
                                        type="password"
                                        name="pass"
                                        
                                        placeholder="Enter password"
                                      />
                                    </div>
                                    <span className="error">
                                      <ErrorMessage name="pass" />
                                    </span>
                                  </div>
                                </div>

                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Confirm Password *
                                    </label>
                                    <div className="password-in">
                                      <Field
                                        id="password-field"
                                        autoComplete="off"
                                        type="password"
                                        name="con_pass"
                                        
                                        placeholder="Enter confirm password"
                                      />
                                    </div>
                                    <span className="error">
                                      <ErrorMessage name="con_pass" />
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-12">
                                  <h4 className="registr_head">
                                    Application details
                                  </h4>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">First Name * </label>
                                    <Field
                                      type="text"
                                      name="fname"
                                      placeholder="Enter here"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="fname" />
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">Last Name * </label>
                                    <Field
                                      type="text"
                                      name="lname"
                                      placeholder="Enter here"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="lname" />
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Business Name (If Applicable)
                                    </label>
                                    <Field
                                      type="text"
                                      name="bname"
                                      placeholder="Enter business name "
                                    />
                                  </div>
                                </div>

                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Vat Number (If Applicable)
                                    </label>
                                    <Field
                                      type="text"
                                      name="vn"
                                      placeholder="Enter here"
                                    />
                                  </div>
                                </div>

                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Company Registration No (If Applicable)
                                    </label>
                                    <Field
                                      type="text"
                                      name="crn"
                                      placeholder="Enter here"
                                    />
                                  </div>
                                </div>

                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Contact Number *
                                    </label>
                                    <Field
                                      type="text"
                                      name="contact_phone"
                                      placeholder="Enter here"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="contact_phone" />
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-12">
                                  <h4 className="registr_head">
                                    Customer address
                                  </h4>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Street Address *
                                    </label>
                                    <Field
                                      type="text"
                                      name="street_address"
                                      placeholder="Enter here"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="street_address" />
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      Address Line 2{" "}
                                    </label>
                                    <Field
                                      type="text"
                                      name="address2"
                                      placeholder="Enter here"
                                    />
                                  </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">Town/City *</label>
                                    <Field
                                      type="text"
                                      name="city"
                                      placeholder="Enter here"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="city" />
                                    </span>
                                  </div>
                                </div>

                                <div className="col-lg-4 col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">Postcode *</label>
                                    <Field
                                      type="text"
                                      name="postcode"
                                      placeholder="Enter here"
                                      
                                    />
                                    <span className="error">
                                      <ErrorMessage name="postcode" />
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-12">
                                  <h4 className="registr_head">
                                    Clinical Oversight
                                  </h4>
                                </div>
                                <div className="col-md-12 col-sm-12">
                                  <div className="input_fields">
                                    <h2>I confirm Clinical Oversight *</h2>
                                    <label className="list_checkBox">
                                      <p>
                                        {" "}
                                        To enable Maxsthetica to be at the
                                        forefront of efforts to minimise risks
                                        to public health the company is
                                        outlining what levels if clinical
                                        oversight it requires to be able to
                                        supply dermal fillers. Clinical
                                        oversight is the term used to describe
                                        patient care activities performed by
                                        supervisors to ensure quality of care.
                                        Clinical oversight (CO) is the term used
                                        to describe the relationship between a
                                        non-medically trained aesthetic
                                        practitioner and someone who is
                                        medically trained and is available to
                                        oversee the activities of the
                                        practitioner if and when required. All
                                        aesthetic practitioners MUST have
                                        clinical oversight while performing
                                        treatments and procedures. This means
                                        all aesthetic practitioners should have
                                        access to help and advice from a
                                        qualified medical professional. This
                                        should be a nurse or a doctor or a
                                        healthcare professional who has
                                        sufficient knowledge of dealing with
                                        adverse effects that could be caused by
                                        aesthetic procedures. Clinical oversight
                                        is vital to ensure patient safety and
                                        public confidence. It is also essential
                                        to provide medical support should any
                                        serious adverse effects be observed
                                        during aesthetic procedures. Three
                                        levels of CO are recommended: 1. CO
                                        Level 1: Clinical oversight is present
                                        in the form of a doctor or a qualified
                                        nurse at all times at the premises where
                                        aesthetic treatments are carried out. 2.
                                        CO Level 2: Clinical oversight is
                                        available from a doctor or a nurse
                                        within 60-minutes-drive or 30 miles of
                                        the premises where the aesthetic
                                        treatments are carried out. The
                                        availability and contact details of the
                                        nominated clinical oversight
                                        professional are noted and displayed at
                                        the premises as the “On Call Medic”. If
                                        no “On Call Medic” is available, then no
                                        procedures should be carried out. 3. CO
                                        Level 3: Where no clinical oversight is
                                        available as per CO Level 2, procedures
                                        should only be carried out if prior
                                        arrangements have been made with a
                                        qualified medical professional to be
                                        available for an internet or video
                                        consultation should it be required. This
                                        should be pre-arranged and noted and
                                        displayed as “Remote On Call Medic”.
                                        With all levels the name and contact
                                        details of the medic present or on call
                                        would be listed with contact details and
                                        prominently displayed at reception
                                        and/or in treatment rooms and must be
                                        advised to Maxsthetica International
                                        Limited on all order forms. I Confirm
                                        Clinical Oversight{" "}
                                      </p>
                                      <Field type="checkbox" name="checkbox2" />
                                      <span className="list_checkmark new_achec"></span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <span className="error">
                                <ErrorMessage name="checkbox2" />
                              </span>
                              <div className="row">
                                <div className="col-12">
                                  <div className="list_registration_qus">
                                    <h4>
                                      Please advise which level of Clinical
                                      Oversight you currently have: *
                                    </h4>

                                    <div className="check_lists">
                                      {this.state.oversite_data.map((item,index)=>(
                                        <label className="list_checkBox">
                                        {item.clinical_oversight}
                                        <input
                                          type="checkbox"
                                          id={item.id}
                                          value={item.id}
                                          name="oversight_id2"
                                          onChange={(event)=> {
                                            { values.oversight_id.includes(event.target.value.toString())?
                                             
                                              values.oversight_id = values.oversight_id.filter(item => item !== event.target.value.toString()):
                                             values.oversight_id.push(event.target.value); console.log("---=====---",values.oversight_id.includes(event.target.value))}
                                            //  values.oversight_id.filter(item => item !== event.target.value) :
                                            //    values.oversight_id.push(item.id) ;

                                            
                                            // console.log("======", values.oversight_id)
                                            setFieldValue("oversight_id2", item.id)}}

                                        />
                                        <span className="list_checkmark"></span>
                                      </label> 
                                      ))}
                                    
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <span className="error">
                                <ErrorMessage name="oversight_id2" />
                              </span>
                              <div className="row">
                                <div className="col-12">
                                  <div className="list_registration_qus">
                                    <h4>Are you a member of the ACPB *</h4>

                                    <div className="check_lists">
                                      <label className="list_checkBox">
                                        Yes
                                        <Field
                                          type="radio"
                                          value="Y"
                                          name="yes"
                                        />
                                        <span className="list_checkmark"></span>
                                      </label>
                                      <label className="list_checkBox">
                                        No (If NO provide details in box
                                        provided of CO)
                                        <Field
                                          type="radio"
                                          value="N"
                                          name="yes"
                                        />
                                        <span className="list_checkmark"></span>
                                      </label>
                                    </div>
                                    <span className="error">
                                      <ErrorMessage name="yes" />
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-md-12 col-sm-12">
                                  <div className="input_fields mt-2">
                                    <label htmlFor="name">
                                      Details of CO provider (Name, Address,
                                      Contact, Reg Num)
                                    </label>
                                    <Field as="textarea"
                                      placeholder="Enter details of CO provider"
                                      name="co_provider"
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-md-12 col-sm-12">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      List Training & Qualifications *
                                    </label>
                                    <Field
                                      placeholder="Enter list training & qualifications"
                                      as="textarea"
                                      name="courses"
                                    />
                                    <span className="error">
                                      <ErrorMessage name="courses" />
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">
                                      List Training & Qualifications *
                                    </label>
                                    <div className="uplodimgfilnew">
                                    <input type="file" id="file1" className="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple=""
                                            onChange={(event)=>  {if(values.file1.length<10){for(let i=0;i<event.target.files.length;i++){values.file1.push(event.target.files[i])};
                                            setFieldValue("file11",values.file1)}}} />
                                      <label htmlFor="file1">
                                        <img
                                          src={uploadnews}
                                          alt=""
                                          className="hoven"
                                        />
                                        <img
                                          src={uploadnews1}
                                          alt=""
                                          className="hoveb"
                                        />
                                        <p>
                                          Drop your file here or click here to
                                          upload{" "}
                                        </p>
                                        <span>
                                          You can upload up to 10 files
                                        </span>
                                      </label>
                                    </div>
                                    {values.file11 ? 
                                          values.file11.map((item,index)=>(
                                             <div style={{flexDirection:'column',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                             <div style={{height:10,width:10,borderRadius:5,backgroundColor:'#f673c1',marginBottom:-20,marginRight:10,alignSelf:'center'}}/>
                                             <div style={{alignSelf:'center',marginLeft:20}}>{item.name}</div>
                                             </div>                                          ))
                                          : null}
                                    <span className="error">
                                      <ErrorMessage name="file11" />
                                    </span>
                                  </div>
                                </div>

                                <div className="col-sm-6">
                                  <div className="input_fields">
                                    <label htmlFor="name">Proof of Id * </label>
                                    <div className="uplodimgfilnew">
                                    <input type="file" id="file2" className="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple=""
                                                onChange={(event)=> {if(values.file2.length<10){for(let i=0;i<event.target.files.length;i++){values.file2.push(event.target.files[i])};
                                                setFieldValue("file22",values.file2)}}} />
                                      <label htmlFor="file2">
                                        <img
                                          src={uploadnews}
                                          alt=""
                                          className="hoven"
                                        />
                                        <img
                                          src={uploadnews1}
                                          alt=""
                                          className="hoveb"
                                        />
                                        <p>
                                          Drop your file here or click here to
                                          upload{" "}
                                        </p>
                                        <span>
                                          You can upload up to 10 files
                                        </span>
                                      </label>
                                    </div>
                                    {values.file22 ? 
                                          values.file22.map((item, index) => ( 
                                             <div style={{flexDirection:'column',justifyContent:'center',alignItems:'center',marginTop:10}}>
                                             <div style={{height:10,width:10,borderRadius:5,backgroundColor:'#f673c1',marginBottom:-20,marginRight:10,alignSelf:'center'}}/>
                                             <div style={{alignSelf:'center',marginLeft:20}}>{item.name}</div>
                                             </div>
                                          )) 
                                          : null}
                                    <span className="error">
                                      <ErrorMessage name="file22" />
                                    </span>
                                  </div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-12">
                                  <h4 className="registr_head">
                                    Practitioner Confirmation
                                  </h4>
                                </div>
                                <div className="col-md-12 col-sm-12">
                                  <div className="input_fields">
                                    <h2>I am a registered Practitioner *</h2>
                                    <label className="list_checkBox">
                                      <p>
                                        {" "}
                                        I, the undersigned, am a registered
                                        Practitioner. I will personally
                                        undertake the ordering of any medicinal
                                        product using the assigned Maxsthetica
                                        International protocol. I, the
                                        practitioner acting on behalf of the
                                        clinic or customer, am prepared to
                                        undertake the responsibility to ensure
                                        that the product is received, stored and
                                        used as stated in the legislation. I
                                        agree to the company Terms & Conditions
                                        and Privacy Statement.{" "}
                                      </p>
                                      <Field type="checkbox" name="checkbox" />
                                      <span className="list_checkmark new_achec"></span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <span className="error">
                                <ErrorMessage name="checkbox" />
                              </span>
                              <div className="row">
                                <div className="col-12">
                                  <div className="border-lins"></div>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-12">
                                  <div className="text-center">
                                    <button type="submit" className="subm_btns">
                                      Submit
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <FormikErrorFocus
                              offset={0}
                              align={"middle"}
                              focusDelay={200}
                              ease={"linear"}
                              duration={1000}
                            />
                          </Form>
                        )}
                      </Formik>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
     
     :
    
           <div className="loader_img">
           <div className="container">
            <div className="row">
               <div className="loader_divs">
                   <div className="scs_msg">
                      <img src="images/success.png"/>
                      <h2>Success!</h2>
                      <p>{this.state.resStatus} </p>
                      <Link to="/login">Go to Login</Link>
                   </div>
                </div>
              </div>
            </div>
        </div>  
     }
     
     
      </Layout>
    );
  }
}
var mapDispatchToProps = (dispatch) => {
  return {
   updateSuccess: (msg)=> dispatch({type: UPDATE_SUCCESS, value: msg})  ,   
   updateError: (msg)=> dispatch({type: UPDATE_ERROR, value: msg}) ,     
   updateLoader: (msg)=> dispatch({type: UPDATE_LOADER, value: msg}) ,    

  }
}

export default connect(null,mapDispatchToProps)(RegistrationNonHealthcareProfessional);