import React, { Component, useState } from "react";
import Layout from "../layout/Layout";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import { ErrorMessage, Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormikErrorFocus from "formik-error-focus";
import axios from "../../common/axios";

import success from "../../assets/images/success.png";
import err from "../../assets/images/err.png";


class RegistrationNonHealthcareProfessional extends Component {
  constructor(props, context) {
		super(props, context)
		this.state = {
      loader:false,
      result_loader:"",
		}
	}

  componentDidMount(){
    console.log("=====",this.props.match.params.code)
    this.setState({loader:true})
    var data= {
      "params": {
        "v_code": this.props.match.params.code
      }
   }

    // axios.post("https://phpwebdevelopmentservices.com/development/maxsthetica_code/api/emailVerificationComplete",
    //  data)
     axios.post("emailVerificationComplete",data)

     .then((response) => {
      console.log("==", response.data)

      this.setState({loader:false})


      if(response.data.error){
        // props.history.push("/reg2/"+values.mobile);
        this.setState({result_loader:"false"})

      }else{
        this.setState({result_loader:"true"})

      }
      
    })
 
    
  }
  render() {
    return (
      <Layout>
        <div className="mar_top"></div>
        {this.state.result_loader==""?
     <div className="main_wrapper">
        {this.state.loader ?
  <div className="loader_img">
	

	<div className="container">
    <div className="row">
    	<div className="loader_divs">
        <div className="loader4"></div>
        </div>
      </div>
    </div>
</div>  :null}
         
        </div>
     
     :
     <>
           {this.state.result_loader=="true"? 
           <div className="loader_img">
           <div className="container">
            <div className="row">
               <div className="loader_divs">
                   <div className="scs_msg">
                      <img src={success}/>
                      <h2>Success!</h2>
                      <p>Your email is verified successfully. Now you can Sign in with your email/username and password. </p>
                      <Link to="/login">Go to Login</Link>
                   </div>
                </div>
              </div>
            </div>
        </div>  
        :null }
              {this.state.result_loader=="false"? 
          <div className="loader_img">
          <div className="container">
           <div className="row">
              <div className="loader_divs">
                  <div className="scs_msg">
                     <img src={err}/>
                     <h2 className="eroor">Error</h2>
                     <p>our verification link has been expired. </p>
                     <Link to="/login">Go to Login</Link>
                  </div>
               </div>
             </div>
           </div>
       </div>  
        :null }
     </>}
     
     
      </Layout>
    );
  }
}
export default RegistrationNonHealthcareProfessional;
