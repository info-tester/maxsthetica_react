import React,{Component} from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';
import {ErrorMessage, Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { connect } from "react-redux";
import Message from '../layout/Message';
import Loader from '../layout/Loader';
import axios from "../../common/axios"
import {UPDATE_SUCCESS, UPDATE_ERROR,UPDATE_LOADER,UPDATE_ID,UPDATE_TOKEN} from "../../store/action/Actions"

 const SignupSchema = Yup.object().shape({
   username: Yup.string()
   //   .min(2, 'Too Short!')
   //   .max(50, 'Too Long!')
     .required('Please enter user name or email id'),
   pass: Yup.string()
   //   .min(8, 'Too Short!')
   //   .max(50, 'Too Long!')
     .required('Please enter password'),
 });
    
 class Login extends Component {
   constructor(props, context) {
		super(props, context)
		this.state = {
      loader:false,
		}
	}

componentDidMount(){
   var data= localStorage.getItem("token");

}
render(){

  return (
   <Layout>
     <div className="mar_top"></div>

     <div className="main_wrapper">
   
        {/* {this.state.loader ?
   <div className="loader_img_new">
	

	<div className="container">
    <div className="row">
    	<div className="loader_divs">
        <div className="loader4"></div>
        </div>
      </div>
    </div>
</div>:null} */}
         <section className="login_sec">
            <div className="container">

               <div className="row">
                  <div className="col-12">
                     <div className="main-center-div">
                        <div className="upper-login">
                     <Message/>
                        	<div className="login_heading">                        
        
	                           <h2>Login</h2>
	                           <p>Welcome Back, Please login to continue</p>
	                        </div>
                           <Formik
                              initialValues={{
                                 username: localStorage.getItem("id"),
                                 pass: localStorage.getItem("pass"),
                                 remember_me: false,
                                 eye: false
                              }}
                              validationSchema={SignupSchema}
                              onSubmit={values => {
                                 // same shape as initial values
                                 // console.log("==========",values);
                                 this.props.updateLoader(true)

                                 var data= {
                                    "params": {
                                      "email": values.username,
                                      "password": values.pass
                                    }
                                  }
                              
                                 axios.post("auth/login",data)
             
                                   .then((response) => {
                                    // console.log("==", response.data)
                                    // if(response.data.result.error[0].username)
                              
                                    this.props.updateLoader(false)
                                    // console.log(response.data.result)
                                    if(response.data.access_token){

                                       if(values.remember_me){
                                          localStorage.setItem("id",values.username)
                                          localStorage.setItem("pass",values.pass)

                                       }

                                       localStorage.setItem("token",response.data.access_token)
                                       localStorage.setItem("user",response.data.userdata)
                                       localStorage.setItem("name",response.data.userdata.username)

                                       this.props.updateToken(response.data.access_token)
                                       this.props.updateID(response.data.userdata)
                                       this.props.updateSuccess("")
                                       this.props.history.push("/dashboard");
                                    }else{
                                    this.props.updateError(response.data.result.status)
                                    setTimeout(() => { this.props.updateError(""); 
                                  }, 5000);
                                    }
                                      // props.history.push("/reg2/"+values.mobile);
                              
                                    
                                  })

                              }}>

                              {({ errors, touched,values, setFieldValue }) => (
                                 <Form>
                           
                              <div className="input-from">
                                 <div className="input_fields">
                                 	<label >Username or Email Address</label>
                                 	<Field id="username" type="text" name="username" placeholder="Enter Username or Email Address"
                                  />
                                  <span className="error">
                                      <ErrorMessage name="username"/>          
                                  </span>                         
                                 </div>

                                 <div className="input_fields">                                 	
                                 	<label >Password</label>
                                 	<div className="password-in">
                                    {!values.eye?
                                    <>
                                    <Field id="password-field"  type="password" name="pass"  required="" placeholder="Enter password"/>
                                 	<div className="input-icon" >
			                           <span toggle="#password-field" id="pass" name="eye" className="field-icon icofont-eye-alt toggle-password"
                                    onClick={(event)=> {setFieldValue("eye", !values.eye)}}/>
			                           </div>
                                    </> :
                                     <>
                                     <Field id="password-field"  type="text" name="pass"  required="" placeholder="Enter password"/>
                                     <div className="input-icon" >
                                     <span toggle="#password-field" id="pass" name="eye" className="field-icon icofont-eye-blocked toggle-password"
                                     onClick={(event)=> {setFieldValue("eye", !values.eye)}}/>
                                     </div>
                                     </>}

                                   <span className="error">
                                      <ErrorMessage name="pass"/>          
                                  </span>   
                                 	</div>
                                 </div>
                                 
                                 <div className="remmber-area">
                                    <label className="list_checkBox">Remember Me
                                    <Field type="checkbox" name="remember_me"/>
                                    <span className="list_checkmark"></span>
                                    </label>
                                    <a className="forgot-passwords" href="#">Lost your password?</a>
                                 </div>
                                 <div className="sing_button">
                                    <button type="submit" className="see_cc">Submit</button>
                                 </div>
                              </div>
                       
                           </Form>
       )}
     </Formik>
                       
                       
                        </div>
                        <div className="lower-login">
                           <h6>Don’t have an Account? Sign up as a</h6>
                           <div className="signin_btns">
                              <Link to="/registration-healthcare-professional">Healthcare Professional</Link>
                              <Link to="/registration-non-healthcare-professional">Non-Healthcare Professional </Link>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
</Layout>
  );
}
 }
 var mapDispatchToProps = (dispatch) => {
   return {
    updateSuccess: (msg)=> dispatch({type: UPDATE_SUCCESS, value: msg})  ,   
    updateError: (msg)=> dispatch({type: UPDATE_ERROR, value: msg}) ,    
    updateLoader: (msg)=> dispatch({type: UPDATE_LOADER, value: msg}) ,    
    updateToken: (msg)=> dispatch({type: UPDATE_TOKEN, value: msg}) ,  
    updateID: (msg)=> dispatch({type: UPDATE_ID, value: msg})     

   }
 }
 
export default connect(null,mapDispatchToProps)(Login);
