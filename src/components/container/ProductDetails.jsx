import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';
import React, { Component, useState } from 'react';

import prp11 from '../../assets/images/prp11.png';
import prp22 from '../../assets/images/prp22.png';
import prp33 from '../../assets/images/prp33.png';
import prp44 from '../../assets/images/prp44.png';
import proimg1 from '../../assets/images/proimg1.png';
import favorite from '../../assets/images/favorite.png';
import sos_log1 from '../../assets/images/sos_log1.png';
import sos_log2 from '../../assets/images/sos_log2.png';
import sos_log3 from '../../assets/images/sos_log3.png';
import favorite_border from '../../assets/images/favorite_border.png';
import popular_img3 from '../../assets/images/popular_img3.png';
import popular_img7 from '../../assets/images/popular_img7.png';
import popular_img1 from '../../assets/images/popular_img1.png';
import popular_img8 from '../../assets/images/popular_img8.png';
import OwlCarousel from 'react-owl-carousel';
const options = {
	margin: 0,
      nav: true,
      autoplay: true,
      loop: true,
      responsive: {
        0: {
          items: 1
        },
        400: {
          items: 2
        },
        768: {
          items: 3
        },
        991: {
          items: 4
        },
        1000: {
          items: 5
        }
      }
}
class ProductDetails extends Component{
	constructor(props, context) {
		super(props, context)
		this.state = {
			qty:1
		}
	}
	componentDidMount(){
		this.updteNinjaSlider();
	}
	updteNinjaSlider=()=>{
		var nsOptions = {
			sliderId: "ninja-slider",
			transitionType: "fade", //"fade", "slide", "zoom", "kenburns 1.2" or "none"
			autoAdvance: false,
			delay: "default",
			transitionSpeed: 700,
			aspectRatio: "2:1",
			initSliderByCallingInitFunc: false,
			shuffle: false,
			startSlideIndex: 0, //0-based
			navigateByTap: true,
			pauseOnHover: false,
			keyboardNav: true,
		
			license: "b2e981"
		};
		const nslider = new window.NinjaSlider(nsOptions);
		var thumbnailSliderOptions = {
			sliderId: "thumbnail-slider",
			orientation: "vertical",
			thumbWidth: "140px",
			thumbHeight: "70px",
			showMode: 2,
			autoAdvance: true,
			selectable: true,
			slideInterval: 3000,
			transitionSpeed: 900,
			shuffle: false,
			startSlideIndex: 0, //0-based
			pauseOnHover: true,
			initSliderByCallingInitFunc: false,
			rightGap: 0,
			keyboardNav: false,
			mousewheelNav: true,
			before: function(currentIdx, nextIdx, manual) {
				if (typeof nslider != "undefined") nslider.displaySlide(nextIdx);
			},
			license: "mylicense"
		};
		const test2 = new window.ThumbnailSlider(thumbnailSliderOptions);
	}
 render(){
  return (
   <Layout>
   <div className="mar_top"></div> 
	<div className="body_bg_product">
	<div className="product-details-sec">
		<div className="container">
			<div className="product-details-inr">
				<div className="br_camp">
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><a href="#url">medicine</a></li>
						<li><span>Budetrol 200</span></li>
					</ul>
				</div>
				<div className="product_details_panel">
					<div className="product_details_left">
						<div className="product_details_left_inr">
							<div className="ninja_trail_remove">
								<div className="left_slider_area">
									<div id="thumbnail-slider" style={{float:'left'}}>
										<div className="inner">
											<ul>
												<li><a className="thumb" href={prp11}></a></li>
												<li><a className="thumb" href={prp22}></a></li>
												<li><a className="thumb" href={prp33}></a></li>
		                                        <li><a className="thumb" href={prp44}></a></li>
											</ul>
										</div>
									</div>
									<div id="ninja-slider" style={{float:'left'}}>
										<div className="slider-inner">
											<ul>
												<li><a className="ns-img" href={proimg1}></a></li>
												<li><a className="ns-img" href={proimg1}></a></li>
												<li><a className="ns-img" href={proimg1}></a></li>
		                                        <li><a className="ns-img" href={proimg1}></a></li>
											</ul>
		                                    <div className="hhert"><a href="#url"><img src={favorite} alt=""/></a></div>
										</div>
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div className="product_details_right">		
						<h1>Budetrol 200 Rotacap 30'S</h1>
						<p>Asthma/COPD </p>
						<b>By Macleods pharmaceuticals</b>	
						<ul className="compostion_sec">
							<li>
								<p>Composition</p>
								<b><u>ASCORBIC ACID-500MG</u></b>
							</li>
							<li>
								<p>Quantity </p>
								<b>30 Rotacap(s) in Box</b>
							</li>
						</ul>
						<div className="sele_numb_bx">
							<div className="sele_numb_left">
								<select>
									<option>250 ml</option>
									<option>150 ml</option>
								</select>
							</div>
							<div className="product-quantity">
	                           <div className="quantity-selectors">
	                              <button type="button" className="decrement-quantity" aria-label="Subtract one" data-direction="-1" 
								  onClick={()=>{if(this.state.qty>1) { this.setState({qty: this.state.qty-1})}  }}><span>&#8722;</span></button>
	                              <input data-min={1} data-max={10} type="text" name="qty" value={this.state.qty} readonly="true"/>
	                              <button type="button" className="increment-quantity" aria-label="Add one" data-direction="1"
								  onClick={()=> {if(this.state.qty<10) { this.setState({qty: this.state.qty+1})}  } }><span>&#43;</span></button>
	                              <div className="clearfix"></div>
	                           </div>
	                        </div>
						</div>	
						<em className="get_off">Get 20% off</em>
						<h4 className="inclusive_tex"><del>£100</del>  £400 <small>(* Inclusive of all taxes)</small></h4>	
						<div className="product_details_right_foot">
							<Link to="/cart" className="pag_arwbtn">Add To Cart</Link>
							<div className="share_cc">
								<span>Share this product :  </span>
								<ul>
									<li><a href="#" translate="_blank"><img src={sos_log1}/></a></li>
									<li><a href="#" translate="_blank"><img src={sos_log2}/></a></li>
									<li><a href="#" translate="_blank"><img src={sos_log3}/></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div className="product-deta_tex_sec">
		<div className="container">
			<div className="deta_tex_inr">
				<h4>About Budetrol 200 Rotacap 30'S</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
				<h4>Warning and Precautions</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.</p>
				<h4>Medicinal Benefits</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
				<h4>Directions for Use</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
				<h4>Storage</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy .</p>
				<h4>Side Effects</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.</p>
			</div>
		</div>
	</div>
	<div className="popular_products_sec rel_products_sec">
		<div className="container">
			<div className="pag_hed">
				<h3>Related Products</h3>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has<br/> been the industry's standard dummy .</p>
			</div>
			<div className="popular_products_inr">
				<div className="rel_products_slid">
					<OwlCarousel   {...options} >
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
								<em><img src={popular_img3} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img7} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
								<em><img src={popular_img1} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img8} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img7} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
								<em><img src={popular_img3} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img7} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
								<em><img src={popular_img1} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img8} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img7} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
								<em><img src={popular_img3} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img7} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
								<em><img src={popular_img1} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img8} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
						<div className="item">
							<div className="popular_products_bx">
								<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
								<a href="#url" className="fev"><img src={favorite} alt=""/></a>
								<em><img src={popular_img7} alt=""/></em>
								<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
								<p>Best price  <del>£100</del> - <b>£400</b></p>
								<a href="#" className="pag_arwbtn">View more</a>
							</div>
						</div>
					</OwlCarousel>
				</div>
			</div>
		</div>
	</div>
</div>

</Layout>

  );
}
}

export default ProductDetails;
