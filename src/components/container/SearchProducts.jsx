import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import ReactPaginate from 'react-paginate';
import FormikErrorFocus from 'formik-error-focus'
import * as Yup from 'yup';
import { ErrorMessage, Formik, Form, Field } from 'formik';
import React, { Component, useState } from 'react';
// import Slider from 'react-rangeslider'
// import 'react-rangeslider/lib/index.css'
import Slider from "@material-ui/core/Slider";
import axios from "../../common/axios"
import  {BASE_URL}  from "../../common/store";

import filter from '../../assets/images/filter.png';
import filtar_img from '../../assets/images/filtar_img.png';
import favorite_border from '../../assets/images/favorite_border.png';
import favorite from '../../assets/images/favorite.png';
import popular_img3 from '../../assets/images/popular_img3.png';
import popular_img4 from '../../assets/images/popular_img4.png';
import popular_img6 from '../../assets/images/popular_img6.png';
import popular_img1 from '../../assets/images/popular_img1.png';
import popular_img7 from '../../assets/images/popular_img7.png';
import popular_img8 from '../../assets/images/popular_img8.png';
import popular_img9 from '../../assets/images/popular_img9.png';



class SearchProduct extends Component {
	constructor(props, context) {
		super(props, context)
		this.state = {
			search_data: [], 
			pageno:0,
			value:[499,15000]
		}
	}
	handleChange = (event, newValue) => {
		this.setState({
			value: newValue
			  })
	  };

	handlePageClick = (n) => {
		console.log("number===", n.selected)
		this.setState({
			pageno: n.selected
			  })
	}



  componentDidMount(){

	console.log("token=====",localStorage.getItem("token"))
	var data= {
		"params": {
		  "keywords": "",
		  "category": "",
		  "manufacturer": "",
		  "max_price": "",
		  "min_price": "",
		  "page_type": "L"
		}
	  }
    axios.get("get-search-product-result",data)

    .then(resp => {

    this.setState({search_data: resp.data.details});

	// console.log(this.state.search_data)

});
  }

	render() {

		return (

			<Layout>
				<div className="mar_top"></div>


				<div className="search_fitar_sec">
					<div className="container">
						<div className="search_fitar_inr">
							<div className="mobile-list" id="mobile-list">
								<p><i className="fa fa-filter"></i> Show Filter</p>
							</div>
							<b className="filter_cc"><img src={filter} alt="" />Filters</b>
							<div className="search_filtar_inr" id="filter_bx">
								<Formik
									initialValues={{
										keywords: '',
										brand: '',
										category: '',
										
									}}
									onSubmit={values => {
										console.log("==========", values," "+"range1:"+this.state.value[0]+" "+"range2:"+this.state.value[1]);
									}}
								>
									{({ errors, touched, values, setFieldValue }) => (
										<Form>
											<div className="row">
												<div className="col-sm-2">
													<div className="input-field">
														<Field type="text" id="name" name="keywords"  />
														<label htmlFor="company"> Keyword</label>
													</div>
												</div>
												<div className="col-sm-2">
													<div className="filtar_input">
														<div className="filtar_drop" id="category_click">
															<span>Category</span>
														</div>
														<div className="category_panel" id="category_div">
															<div className="category_checkBox">
																<input id="chk1" type="checkbox" name="category" 
																onChange={(event)=> {setFieldValue("category", "Fitness & Supplements")}}/>

																<label htmlFor="chk1">Fitness & Supplements</label>
															</div>
															<div className="category_checkBox">
																<input id="chk2" type="checkbox" name="category" 
																  onChange={(event)=> {setFieldValue("category", "Health Care")}}/>
																<label htmlFor="chk2">Health Care</label>
															</div>
															<div className="category_checkBox">
																<input id="chk3" type="checkbox" name="category"
																 onChange={(event)=> {setFieldValue("category", "Medicines")}}/>

																<label htmlFor="chk3">Medicines</label>
															</div>
															<div className="category_checkBox">
																<input id="chk4" type="checkbox" name="category" 
																onChange={(event)=> {setFieldValue("category", "Abc test ctegory")}}/>

																<label htmlFor="chk4">Abc test ctegory</label>
															</div>
															<div className="category_checkBox">
																<input id="chk5" type="checkbox" name="category" 
																onChange={(event)=> {setFieldValue("category", "All Category")}}/>
																<label htmlFor="chk5">All Category</label>
															</div>
															<div id="moretext" style={{ display: "none" }}>
																<div className="category_checkBox">
																	<input id="chk6" type="checkbox" name="category" 
																	onChange={(event)=> {setFieldValue("category", "Fitness & Supplements")}}/>
																	<label htmlFor="chk6">Fitness & Supplements</label>
																</div>
																<div className="category_checkBox">
																	<input id="chk7" type="checkbox" name="category" 
																	onChange={(event)=> {setFieldValue("category", "Medicine")}}/>
																	<label htmlFor="chk7">Medicine</label>
																</div>
																<div className="category_checkBox">
																	<input id="chk8" type="checkbox" name="category" 
																	onChange={(event)=> {setFieldValue("category", "Health Care")}}/>
																	<label htmlFor="chk8">Health Care</label>
																</div>
															</div>
															<em href="#url" id="moreless-button" className="more_cc">+ More</em>
														</div>
													</div>
												</div>
												<div className="col-sm-2">
													<div className="filtar_input">
														<div className="filtar_drop" id="brand_click">
															<span>Brand / manufacturers</span>
														</div>
														<div className="category_panel" id="brand_div">
															<div className="category_checkBox">
																<input id="chk9" type="checkbox"  name="brand" 
																onChange={(event)=> {setFieldValue("brand", "Pfizer")}}/>
																<label htmlFor="chk9">Pfizer </label>
															</div>
															<div className="category_checkBox">
																<input id="chk10" type="checkbox"  name="brand" 
																onChange={(event)=> {setFieldValue("brand", "Colgate")}}/>
																<label htmlFor="chk10">Colgate</label>
															</div>
															<div className="category_checkBox">
																<input id="chk11" type="checkbox" name="brand" 
																onChange={(event)=> {setFieldValue("brand", "Roche")}}/>
																<label htmlFor="chk11">Roche </label>
															</div>
															<div className="category_checkBox">
																<input id="chk12" type="checkbox"  name="brand" 
																onChange={(event)=> {setFieldValue("brand", "Johnson & Johnson")}}/>
																<label htmlFor="chk12">Johnson & Johnson</label>
															</div>
															<div className="category_checkBox">
																<input id="chk13" type="checkbox"  name="brand" 
																onChange={(event)=> {setFieldValue("brand", "GlaxoSmithKline")}}/>
																<label htmlFor="chk13">GlaxoSmithKline </label>
															</div>
															<div id="moretext1" style={{ display: "none" }}>
																<div className="category_checkBox">
																	<input id="chk14" type="checkbox"  name="brand" 
																	onChange={(event)=> {setFieldValue("brand", "Pfizer")}}/>
																	<label htmlFor="chk14">Pfizer</label>
																</div>
																<div className="category_checkBox">
																	<input id="chk15" type="checkbox"  name="brand" 
																	onChange={(event)=> {setFieldValue("brand", "Roche")}}/>
																	<label htmlFor="chk15">Roche</label>
																</div>
																<div className="category_checkBox">
																	<input id="chk16" type="checkbox"  name="brand" 
																	onChange={(event)=> {setFieldValue("brand", "Colgate")}}/>
																	<label htmlFor="chk16">Colgate</label>
																</div>
															</div>
															<em href="#url" id="moreless-button1" className="more_cc">+ More</em>
														</div>
													</div>
												</div>
												<div className="col-sm-2">
													<div className="slider_rnge">


														{/* <Slider
															min={0}
															max={15000}
															step={1}
															
															// value={[100, 8000]}
															value={this.state.volume1}
															orientation="horizontal"
															onChange={this.handleOnChange}

															// reverse={true}
															// tooltip={true}
															// format={this.handleOnChange}
															// onChangeStart={this.handleOnChange}
															// onChangeComplete={this.handleOnChange}
														/> */}

<Slider  min={0} max={15000} step={1}  value={this.state.value} onChange={this.handleChange} />

														{/* <div id="slider-range" className="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"> */}

														{/* <div className="ui-slider-range ui-widget-header ui-corner-all" style={{left: '0%', width: '100%'}}></div> 
								<span tabIndex="0" className="ui-slider-handle ui-state-default ui-corner-all" style={{left: '3.32667%'}}></span> 
								<span tabIndex="0" className="ui-slider-handle ui-state-default ui-corner-all" style={{left: '66.6667%'}}></span> 
								<div className="ui-slider-range ui-corner-all ui-widget-header" style={{left: '3.32667%', width: '63.34%'}}>
									</div> */}
														{/* </div>  */}





														<div className="prin_rance">
															<b>Price Range :</b>
															<span className="range-text"><input type="text" className="price_numb" value={"£"+this.state.value[0]+" - "+"£"+this.state.value[1]}  id="amount" /></span>
														</div>
													</div>
												</div>
												<div className="col-sm-2 fil_btn_wi">
													<div className="filter_btn_bx">
														<button type="submit" className="filter_btn"><img src={filtar_img} alt="" />Apply filter</button>
													</div>
												</div>
											</div>
											
										</Form>
									)}
								</Formik>
							</div>
						</div>
					</div>
				</div>

				<div className="search_sec">
					<div className="container">
						<div className="search_inr">
							<div className="search_hed">
								<h1>Displaying (1 - {this.state.search_data.length<50 ? this.state.search_data.length: "50"}) of {this.state.search_data.length} - Products </h1>
								<div className="sort_bx">
									<span>Sort by</span>
									<select>
										<option>Price low to high</option>
										<option>Price High to Low</option>
									</select>
								</div>
							</div>
							<div className="search_body">
								<div className="row">

									{this.state.search_data.map((item,index)=>

									<div className="col-sm-2">
									<div className="popular_products_bx">
										<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
										<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
										<em><img src={ BASE_URL+item.get_manufacturer.picture} alt="" /></em>
										<h4>{item.title}</h4>
										<p>Best price  <del>£100</del> - <b>£{item.minimum_price? item.minimum_price:"00"}</b></p>
										<Link to="/product-details" className="pag_arwbtn">View more</Link>
									</div>
									</div>
									 )} 


{/* 

									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img3} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img7} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img1} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img8} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img7} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img6} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img4} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img3} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img6} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img9} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img3} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img7} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img1} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img8} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img7} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img6} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img4} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img3} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img6} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img9} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img3} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img7} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite_border} alt="" /></a>
											<em><img src={popular_img1} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2">
										<div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img8} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div>
									<div className="col-sm-2"> */}
										{/* <div className="popular_products_bx">
											<span className="tool tool_t quti_tool" data-tip="Prescription Required" tabIndex="1"><i className="fa fa-exclamation"></i></span>
											<a href="#url" className="fev"><img src={favorite} alt="" /></a>
											<em><img src={popular_img7} alt="" /></em>
											<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
											<p>Best price  <del>£100</del> - <b>£400</b></p>
											<Link to="/product-details" className="pag_arwbtn">View more</Link>
										</div>
									</div> */}
								</div>
								<div className="pagination_sec">
									<nav aria-label="...">
										<ReactPaginate
											pageClassName="page-item"
											pageLinkClassName="page-link"
											activeClassName="page-item active"
											activeLinkClassName="page-link"
											previousClassName="page-link"
											nextClassName="page-link"
											previousLinkClassName="fa fa-chevron-left"
											nextLinkClassName="fa fa-chevron-right"
											disabledClassName="page-item"
											// disabledLinkClassName="page-link"
											// containerClassName="pagination_sec"
											className="pagination"
											breakClassName="page-link"
											initialPage={1}
											breakLabel="....."
											nextLabel=""
											onPageChange={this.handlePageClick}
											pageRangeDisplayed={3}
											pageCount={10}
											previousLabel=""
											marginPagesDisplayed={1}
											renderOnZeroPageCount={null}

										/>
										{/* <nav aria-label="...">
					  <ul className="pagination">
					    <li className="page-item disabled">
					      <a className="page-link" href="#" tabIndex="-1"><i className="fa fa-chevron-left"></i></a>
					    </li>
					    <li className="page-item"><a className="page-link" href="#">1</a></li>
					    <li className="page-item active">
					      <a className="page-link" href="#">2 <span className="sr-only">(current)</span></a>
					    </li>
					    <li className="page-item"><a className="page-link" href="#">3</a></li>
					    <li className="page-item"><a className="page-link" href="#">4</a></li>
					    <li className="page-item"><a className="page-link" href="#">.....</a></li>
					    <li className="page-item"><a className="page-link" href="#">10</a></li>
					    <li className="page-item">
					      <a className="page-link" href="#"><i className="fa fa-chevron-right"></i></a>
					    </li>
							  </ul> */}
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>

			</Layout>

		);
	}
}

export default SearchProduct;
