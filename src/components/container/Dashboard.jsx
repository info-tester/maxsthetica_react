import React,{Component} from 'react';
import Layout from '../layout/Layout2'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';
import Message from '../layout/Message';

import pro_pick from '../../assets/images/pro_pick.png'
import das_icon1 from '../../assets/images/das_icon1.png'
import das_icon2 from '../../assets/images/das_icon2.png'
import das_icon3 from '../../assets/images/das_icon3.png'
import das_icon4 from '../../assets/images/das_icon4.png'
import das_icon5 from '../../assets/images/das_icon5.png'
import das_icon6 from '../../assets/images/das_icon6.png'
import das_img1 from '../../assets/images/das_img1.png'
import das_img2 from '../../assets/images/das_img2.png'
import das_img3 from '../../assets/images/das_img3.png'
import das_img4 from '../../assets/images/das_img4.png'
import tim from '../../assets/images/tim.png'



    
class Dashboard extends Component{

   constructor(props, context) {
		super(props, context)
		this.state = {
			user_data:{},
         name:''
   
		}
	}

  componentDidMount(){
     var data= localStorage.getItem("name");
     this.setState({name:data})

  };

 render(){
  return (
<Layout>
 <div className="mar_top"></div>
    
 <div className="dashbord_sec">
    <div className="container">
       <Message/>
       <div className="dashbord_inr"> 
          <div className="dashbord_left">
             <div className="mobile_menu" id="mobile_menu"> <i className="fa fa-bars"></i>
                  <span>Show Menu</span>
            </div>
             <div className="dashbord_left_ir" id="mobile_menu_dv">
                <div className="dashbord_left_top">
                   <div className="media">
                      <em><img src={pro_pick} alt=""/></em>
                      <div className="media-body">
                         <h4>{this.state.name}</h4>
                         <p>dummy-email12@gmail.com</p>
                      </div>
                   </div>
                </div>
                <div className="dash_pro_link">
                  <ul>
                      <li><Link to="/dashboard" className="actv"><img src={das_icon1} alt=""/><span>Dashboard </span></Link></li>
                      <li><a href=""><img src={das_icon2} alt=""/><span>Order History</span></a></li>
                      <li><Link to="/address-book"><img src={das_icon3} alt=""/><span>Address Book</span></Link></li>
                      <li><a href=""><img src={das_icon4} alt=""/><span>Payment History</span></a></li>
                      <li><a href=""><img src={das_icon5} alt=""/><span>My Favorites</span></a></li>
                      <li><Link to="/" 
                      onClick={()=> localStorage.setItem("token","")}>
                         <img src={das_icon6} alt=""/><span>Log Out</span></Link></li>
                  </ul>
                </div>
             </div>
          </div>
          <div className="dashbord_right">
             <div className="dashbord_right_ir">
                <h1>Dashboard</h1>
                <div className="dashbord_frm">
                   <div className="dashbord_pg_panel">
                      <div className="dashbord_pg_top">
                         <h6><img src={das_img4} alt=""/> Hi, {this.state.name}</h6>
                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut laibore dolore magna aliqua.labor consectetur adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua text labor labore et dolore magna aliqua.</p>
                      </div>
                      <div className="das_line"></div>
                      <div className="last_order_box">
                         <h5>Last order </h5>
                         <div className="last_order_panel">
                            <div className="last_order_itm">
                               <div className="last_order_left">
                                  <div className="last_order_bx">
                                     <p>Order Number</p>
                                     <span>AB0001</span>
                                  </div>
                                  <div className="last_order_bx">
                                     <div className="order_tm">
                                        <p>Ordered on</p>
                                        <span><img src={tim} alt=""/>14th Feb, 2022</span>
                                     </div>                                    
                                  </div>
                                  <div className="last_order_bx">
                                     <p>Medicine name here</p>
                                     <span>250 ml (2-Box)</span>
                                  </div>
                                  <div className="last_order_bx">
                                     <p>Total : £300</p>
                                     <span> Items : 10</span>
                                  </div>
                                  <div className="last_order_bx stas_dv">
                                     <p>Status</p>
                                     <span className="stas_dv_bg"> Delivered</span>
                                  </div>
                               </div>
                               <div className="last_order_right">
                                  <div className="last_order_inr">
                                     <h4>Items In cart<br/> <span>10</span></h4>
                                     <a href="#url" className="pag_arwbtn">Checkout </a>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div className="total_order_bx">
                         <div className="row">
                            <div className="col-sm-4">
                               <div className="total_order_item">
                                  <div className="media">
                                     <em><img src={das_img1} alt=""/></em>
                                     <div className="media-body">
                                        <p>Total Order</p>
                                        <b>100</b>
                                     </div>
                                  </div>
                                  <div className="progress_bx">
                                     <div className="progress">
                                       <div className="progress-bar" role="progressbar" style={{width: "25%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                            <div className="col-sm-4">
                               <div className="total_order_item">
                                  <div className="media">
                                     <em><img src={das_img2} alt=""/></em>
                                     <div className="media-body">
                                        <p>Awaiting Approval</p>
                                        <b>10</b>
                                     </div>
                                  </div>
                               </div>
                            </div>
                            <div className="col-sm-4">
                               <div className="total_order_item">
                                  <div className="media">
                                     <em><img src={das_img3} alt=""/></em>
                                     <div className="media-body">
                                        <p>Delivered Order</p>
                                        <b>90</b>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>   
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
</Layout>
  );
}
}
export default Dashboard;