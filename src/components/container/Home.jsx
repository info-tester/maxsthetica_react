import React from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';

import banimg from '../../assets/images/banimg.png'
import btn_arw from '../../assets/images/btn_arw.png'
import patient1 from '../../assets/images/patient1.png'
import patient2 from '../../assets/images/patient2.png'
import patient3 from '../../assets/images/patient3.png'
import patient4 from '../../assets/images/patient4.png'
import why_choose1 from '../../assets/images/why_choose1.png'
import why_choose2 from '../../assets/images/why_choose2.png'
import why_choose3 from '../../assets/images/why_choose3.png'
import vedio_img from '../../assets/images/vedio_img.png'
import palay from '../../assets/images/palay.png'
import favorite from '../../assets/images/favorite.png'
import popular_img1 from '../../assets/images/popular_img1.png'
import popular_img2 from '../../assets/images/popular_img2.png'
import popular_img3 from '../../assets/images/popular_img3.png'
import popular_img4 from '../../assets/images/popular_img4.png'
import popular_img5 from '../../assets/images/popular_img5.png'
import popular_img6 from '../../assets/images/popular_img6.png'
import favorite_border from '../../assets/images/favorite_border.png'
import qu1 from '../../assets/images/qu1.png'
import award_winning from '../../assets/images/award_winning.png'
import arrow_rig from '../../assets/images/arrow-rig.png'
import star1 from '../../assets/images/star1.png'
import news1 from '../../assets/images/news1.png'
import news2 from '../../assets/images/news2.png'
import news3 from '../../assets/images/news3.png'
import user from '../../assets/images/user.png'

const Home =(props)=>{
 
  return (
   <Layout>
     <div className="mar_top"></div> 
      <div className="banar_sec">
	<div className="ban_img_sec">
		<div className="container">
			<div className="ban_img_inr">
				<em>
					<img src={banimg} alt=""/>
				</em>
			</div>
		</div>
	</div>
	<div className="ban_tex_sec">	
		<div className="container">
			<div className="ban_tex_inr">
				<h5>World-class Healthcare</h5>
				<h6>Welcome to <br/>Maxsthetica International</h6>
				<p>Maxsthetica is your all-in-one pharmacy, private clinic and aesthetics solution<br/> designed to keep you healthy and looking your best.</p>
				<ul className="ban_ntn">
					<li><a href="#url" className="pag_arwbtn">Sign up <img src={btn_arw} alt=""/></a></li>
					<li><Link to="/registration-healthcare-professional" className="pag_arwbtn">Healthcare Professional  <img src={btn_arw} alt=""/></Link></li>
					<li><Link to="/registration-non-healthcare-professional" className="pag_arwbtn">Non-Healthcare Professional <img src={btn_arw} alt=""/></Link></li>
				</ul>
			</div>
		</div>	
	</div>
</div>
<div className="patint_detl_sec">
	<div className="container">
		<div className="patint_detl_inr">
			<ul>
				<li>
					<div className="media">
						<em><img src={patient1} alt=""/></em>
						<div className="media-body">
							<h3>200000</h3>
							<p>Patients</p>
						</div>
					</div>
				</li>
				<li>
					<div className="media">
						<em><img src={patient2} alt=""/></em>
						<div className="media-body">
							<h3>20</h3>
							<p>Celebrity Patients </p>
						</div>
					</div>
				</li>
				<li>
					<div className="media">
						<em><img src={patient3} alt=""/></em>
						<div className="media-body">
							<h3>8</h3>
							<p>Hours a day </p>
						</div>
					</div>
				</li>
				<li>
					<div className="media">
						<em><img src={patient4} alt=""/></em>
						<div className="media-body">
							<h3>7</h3>
							<p>Days a week </p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

{/* <!-- Why Choose Maxsthetica International  --> */}

<div className="why_choose_sec">
	<div className="container">
		<div className="pag_hed">
			<h1>Why Choose Maxsthetica International </h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
		</div>
	</div>
	<div className="why_choose_panel">
		<div className="container">
			<div className="why_choose_inr">
				<div className="row">
					<div className="col-sm-4">
						<div className="why_choose_bx">
							<em><img src={why_choose1}/></em>
							<h4>Prescription Treatments</h4>
							<p>Our pharmacists are able to prescribe a multitude of prescription-only medicines for a range of conditions. Take a look now!</p>
						</div>
					</div>
					<div className="col-sm-4">
						<div className="why_choose_bx">
							<em><img src={why_choose2}/></em>
							<h4>Aesthetics Clinic</h4>
							<p>Our award-winning clinicians can provide a number of procedures and you can purchase treatments directly online. Find out more!</p>
						</div>
					</div>
					<div className="col-sm-4">
						<div className="why_choose_bx">
							<em><img src={why_choose3}/></em>
							<h4>Online Pharmacy</h4>
							<p>Why go and wait in the pharmacy to buy your medicines? Take a look at what we have in store and order via a few clicks!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div className="why_choose_btn">
		<a href="#url" className="pag_arwbtn">Learn more <img src={btn_arw} alt=""/></a>
	</div>
</div>
{/* <!-- vedio section --> */}
<div className="vedio_sec">
	<div className="container">
		<div className="pag_hed">
			<h2>We provide innovative healthcare<br/> solutions for our clients</h2>
		</div>
		<div className="vedio_inr">
			<div className="videoBox">
				<img src={vedio_img} alt=""/>
				<div className="playIcon">
					<a href="http://www.youtube.com/watch?v=opj24KnzrWo" className="fancybox-media">
						<img src={palay} alt="" rel="media-gallery"/></a>
				</div>
			</div>
		</div>
	</div>
</div>

{/* <!-- Popular Products  --> */}

<div className="popular_products_sec">
	<div className="container">
		<div className="pag_hed">
			<h3>Popular Products</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
		</div>
		<div className="popular_products_inr">
			<div className="popular_products_slid">
				<div className="owl-carousel">
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite} alt=""/></a>
							<em><img src={popular_img1} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img2} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img3} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img4} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img5} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img6} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite} alt=""/></a>
							<em><img src={popular_img1} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img2} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img3} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img4} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img5} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img6} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite} alt=""/></a>
							<em><img src={popular_img1} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img2} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img3} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img4} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img5} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
					<div className="item">
						<div className="popular_products_bx">
							<a href="#url" className="fev"><img src={favorite_border} alt=""/></a>
							<em><img src={popular_img6} alt=""/></em>
							<h4>Pharmeasy Calcium, Magnesium, Zinc & Vitamin D3</h4>
							<a href="#" className="pag_arwbtn">Login to view price</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


{/* <!-- Award-winning service --> */}

<div className="award_winning_sec">
	<div className="container">
		<div className="award_winning_inr">
			<div className="award_winning_img">
				<img src={award_winning} alt=""/>
			</div>
			<div className="award_winning_text">
				<h4>Award-winning service</h4>
				<p>With over 200,000 patients treated, Maxsthetica is a world-renowned<br/> pharmacy and aesthetics clinic.</p>
				<ul>
					<li><a href="#url"><img src={arrow_rig} alt=""/> Prescription treatments</a></li>
					<li><a href="#url"><img src={arrow_rig} alt=""/> Aesthetics treatments</a></li>
					<li><a href="#url"><img src={arrow_rig} alt=""/> Pharmacy medicines</a></li>
					<li><a href="#url"><img src={arrow_rig} alt=""/> Online Doctor</a></li>
					<li><a href="#url"><img src={arrow_rig} alt=""/> Support and advice</a></li>					
				</ul>
			</div>
		</div>
	</div>
</div>


{/* <!-- What Our Clients Say --> */}

<div className="clients_say_sec">
	<div className="container">
		<div className="pag_hed">
			<h3>What Our Clients Say</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
		</div>
		<div className="clients_say_inr">
			<div className="clients_say_slid">
				<div className="owl-carousel">
					<div className="item">
						<div className="clients_bx">
							<em><img src={qu1} alt=""/></em>
							<div className="clients_text">
								<p>Friendly staff and I received a warm welcome when I arrived. The practitioner was very professional and gave me all the information I needed to know about my treatment which did not cause me any discomfort (despite having several injections!). Aftercare was fully explained. I would highly recommend the clinic and I am looking forward to returning for further treatments.</p>
							</div>
							<div className="clients_foot">
								<div className="clients_foot_left">
									<h4>Lynne Blackwell</h4>
									<p>California</p>
								</div>
								<div className="clients_foot_rig">
									<ul>
										<li>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div className="item">
						<div className="clients_bx">
							<em><img src={qu1} alt=""/></em>
							<div className="clients_text">
								<p>I absolutely love it here, the staff are so friendly and helpful, and really take the time to talk with you and advise you on the potential treatments available. Petra was brilliant and I have already booked my next appointment. Highly recommended </p>
							</div>						
							<div className="clients_foot">
								<div className="clients_foot_left">
									<h4>July Foster</h4>
									<p>California</p>
								</div>
								<div className="clients_foot_rig">
									<ul>
										<li>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div className="item">
						<div className="clients_bx">
							<em><img src={qu1} alt=""/></em>
							<div className="clients_text">
								<p>I was 15 minutes late due to traffic on the motorway as I was coming from Birmingham. All the staff were so understanding and kind. Lucy did my lip fillers and she was amazing. Took her time to do them properly and talked me through everything she was doing. Always come here for my treatments. Would recommend highly.</p>
							</div>						
							<div className="clients_foot">
								<div className="clients_foot_left">
									<h4>Beaudene Smith</h4>
									<p>California</p>
								</div>
								<div className="clients_foot_rig">
									<ul>
										<li>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
											<span><img src={star1} alt=""/></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{/* <!-- News section --> */}

<div className="news_sec">
	<div className="container">
		<div className="pag_hed">
			<h4>News</h4>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
		</div>
		<div className="news_inr">
			<div className="row">
				<div className="col-sm-4">
					<div className="news_box">
						<div className="news_img">
							<img src={news1} alt=""/>
							<div className="news_time">
								<h6>17</h6>
								<span>Jul, 2020</span>
							</div>
						</div>
						<div className="news_text">
							<h4><a href="#"> The Benefits Of Undertaking Cosmetic Courses</a></h4>
							<em><img src={user}/> By Maxsthetica International</em>
							<p>There is no denying that cosmetic treatments are incredibly popular here in the UK and across the world. In this day and age, they are much...</p>
							<a href="#" className="pag_arwbtn">Read more<img src={btn_arw} alt=""/></a>
						</div>
					</div>
				</div>
				<div className="col-sm-4">
					<div className="news_box">
						<div className="news_img">
							<img src={news2} alt=""/>
							<div className="news_time">
								<h6>17</h6>
								<span>Jul, 2020</span>
							</div>
						</div>
						<div className="news_text">
							<h4><a href="#"> Why Aesthetic Cosmetic Treatments Are So Popular</a></h4>
							<em><img src={user}/> By Maxsthetica International</em>
							<p>There is no denying that cosmetic treatments are incredibly popular here in the UK and across the world. In this day and age, they are much...</p>
							<a href="#" className="pag_arwbtn">Read more<img src={btn_arw} alt=""/></a>
						</div>
					</div>
				</div>
				<div className="col-sm-4">
					<div className="news_box">
						<div className="news_img">
							<img src={news3} alt=""/>
							<div className="news_time">
								<h6>17</h6>
								<span>Jul, 2020</span>
							</div>
						</div>
						<div className="news_text">
							<h4><a href="#">COVID-19 Antibody Test Available at Hope Medical</a></h4>
							<em><img src={user}/> By Maxsthetica International</em>
							<p>There is no denying that cosmetic treatments are incredibly popular here in the UK and across the world. In this day and age, they are much...</p>
							<a href="#" className="pag_arwbtn">Read more<img src={btn_arw} alt=""/></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{/* <!-- faq section--> */}

<div className="faq_sec">
	<div className="container">
		<div className="pag_hed">
			<h4>Frequently Asked Questions</h4>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br/> industry's standard dummy text ever since the when</p>
		</div>
		<div className="faq_inr">
			<div className="accordian-faq">
				<div className="accordion" id="faq">
					<div className="card">
						<div className="card-header" id="faqhead1">
							<a href="#" className="collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">
								Q. Can you supply products directly to aesthetics clinics?
							</a>
						</div>
						<div id="faq1" className="collapse" aria-labelledby="faqhead1" data-parent="#faq">
							<div className="card-body">
								<p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
							</div>
						</div>
					</div>
					<div className="card">
						<div className="card-header" id="faqhead2">
							<a href="#" className="collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">
								Q. Are you just an aesthetics pharmacy?
							</a>
						</div>
						<div id="faq2" className="collapse" aria-labelledby="faqhead2" data-parent="#faq">
							<div className="card-body">
								<p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
							</div>
						</div>
					</div>
					<div className="card">
						<div className="card-header" id="faqhead3">
							<a href="#" className="collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">
								Q.  Can I order my prescription medication from you?
							</a>
						</div>
						<div id="faq3" className="collapse" aria-labelledby="faqhead3" data-parent="#faq">
							<div className="card-body">
								<p>Maxsthetica International specialises in supplying pharmaceutical products to the aesthetics and beauty industry, so you can order your products through us today!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</Layout>

  );
}

export default Home;
