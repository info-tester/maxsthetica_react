import React from 'react';
import Layout from '../layout/Layout2'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';

import pro_pick from '../../assets/images/pro_pick.png'
import das_icon1 from '../../assets/images/das_icon1.png'
import das_icon2 from '../../assets/images/das_icon2.png'
import das_icon3 from '../../assets/images/das_icon3.png'
import das_icon4 from '../../assets/images/das_icon4.png'
import das_icon5 from '../../assets/images/das_icon5.png'
import das_icon6 from '../../assets/images/das_icon6.png'
import add_img from '../../assets/images/add_img.png'


    
const AddressBook =(props)=>{
 
  return (

<Layout>
<div className="mar_top"></div>
   
   <div className="dashbord_sec">
      <div className="container">
         <div className="dashbord_inr"> 
            <div className="dashbord_left">
               <div className="mobile_menu" id="mobile_menu"> <i className="fa fa-bars"></i>
                    <span>Show Menu</span>
              </div>
               <div className="dashbord_left_ir" id="mobile_menu_dv">
                  <div className="dashbord_left_top">
                     <div className="media">
                        <em><img src={pro_pick} alt=""/></em>
                        <div className="media-body">
                           <h4>John Graham</h4>
                           <p>dummy-email12@gmail.com</p>
                        </div>
                     </div>
                  </div>
                  <div className="dash_pro_link">
                    <ul>
                        <li><Link to="/dashboard"><img src={das_icon1} alt=""/><span>Dashboard </span></Link></li>
                        <li><a href=""><img src={das_icon2} alt=""/><span>Order History</span></a></li>
                        <li><Link to="/address-book" className="actv"><img src={das_icon3} alt=""/><span>Address Book</span></Link></li>
                        <li><a href=""><img src={das_icon4} alt=""/><span>Payment History</span></a></li>
                        <li><a href=""><img src={das_icon5} alt=""/><span>My Favorites</span></a></li>
                        <li><Link to="/"><img src={das_icon6} alt=""/><span>Log Out</span></Link></li>
                    </ul>
                  </div>
               </div>
            </div>
            <div className="dashbord_right">
               <div className="dashbord_right_ir address_book">
                  <div className="add_div_tit">
                      <h1>Address Book</h1>
                      <Link to="/add-address" className="pag_arwbtn add_new_add_btn"><img src={add_img} alt=""/>Add New Address</Link>
                  </div>              
                  <div className="dashbord_frm">
                     <div className="row">
                         <div className="col-sm-6">
                            <div className="address_book_bx">
                               <div className="address_book_top">
                                  <h5><span>Home</span> (Default Shipping Address) </h5>
                                  <ul className="edit_btn">
                                     <li><a href="url" className="ed_bt">Edit</a></li>
                                     <li><a href="url" className="ed_bt ed_bt_bl">Delete</a></li>
                                  </ul>
                               </div>
                               <div className="address_book_body">
                                  <div className="add_book_itam">
                                    <li><span>Name</span><em>Rabin Chatterjee</em></li>
                                    <li><span>Address</span><em>Kolkata, West Bengal - 700064, India</em></li>
                                    <li><span>Phone Number</span><em>03212 4521455</em></li>
                                    <li><span>Mobile Number</span><em>+91 9856 552 523</em></li>
                                    <li><span>Email</span><em>dummyemail55@gmail.com</em></li>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div className="col-sm-6">
                            <div className="address_book_bx">
                               <div className="address_book_top">
                                  <h5><span>Office </span> (Default Shipping Address) </h5>
                                  <ul className="edit_btn">
                                     <li><a href="url" className="ed_bt">Edit</a></li>
                                     <li><a href="url" className="ed_bt ed_bt_bl">Delete</a></li>
                                  </ul>
                               </div>
                               <div className="address_book_body">
                                  <div className="add_book_itam">
                                    <li><span>Name</span><em>Rabin Chatterjee</em></li>
                                    <li><span>Address</span><em>Kolkata, West Bengal - 700064, India</em></li>
                                    <li><span>Phone Number</span><em>03212 4521455</em></li>
                                    <li><span>Mobile Number</span><em>+91 9856 552 523</em></li>
                                    <li><span>Email</span><em>dummyemail55@gmail.com</em></li>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div className="col-sm-6">
                            <div className="address_book_bx">
                               <div className="address_book_top">
                                  <h5><span>Home - 1</span> (Default Shipping Address) </h5>
                                  <ul className="edit_btn">
                                     <li><a href="url" className="ed_bt">Edit</a></li>
                                     <li><a href="url" className="ed_bt ed_bt_bl">Delete</a></li>
                                  </ul>
                               </div>
                               <div className="address_book_body">
                                  <div className="add_book_itam">
                                    <li><span>Name</span><em>Rabin Chatterjee</em></li>
                                    <li><span>Address</span><em>Kolkata, West Bengal - 700064, India</em></li>
                                    <li><span>Phone Number</span><em>03212 4521455</em></li>
                                    <li><span>Mobile Number</span><em>+91 9856 552 523</em></li>
                                    <li><span>Email</span><em>dummyemail55@gmail.com</em></li>
                                  </div>
                               </div>
                            </div>
                         </div>
                         <div className="col-sm-6">
                            <div className="address_book_bx">
                               <div className="address_book_top">
                                  <h5><span>Office - 1</span> (Default Shipping Address) </h5>
                                  <ul className="edit_btn">
                                     <li><a href="url" className="ed_bt">Edit</a></li>
                                     <li><a href="url" className="ed_bt ed_bt_bl">Delete</a></li>
                                  </ul>
                               </div>
                               <div className="address_book_body">
                                  <div className="add_book_itam">
                                    <li><span>Name</span><em>Rabin Chatterjee</em></li>
                                    <li><span>Address</span><em>Kolkata, West Bengal - 700064, India</em></li>
                                    <li><span>Phone Number</span><em>03212 4521455</em></li>
                                    <li><span>Mobile Number</span><em>+91 9856 552 523</em></li>
                                    <li><span>Email</span><em>dummyemail55@gmail.com</em></li>
                                  </div>
                               </div>
                            </div>
                         </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
</Layout>
  );
}

export default AddressBook;
