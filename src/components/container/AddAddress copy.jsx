import React from 'react';
import Layout from '../layout/Layout2'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';



    
const AddAddress =(props)=>{
 
  return (

<Layout>
<div class="mar_top"></div>
   
   <div class="dashbord_sec">
      <div class="container">
         <div class="dashbord_inr"> 
            <div class="dashbord_left">
               <div class="mobile_menu" id="mobile_menu"> <i class="fa fa-bars"></i>
                    <span>Show Menu</span>
              </div>
               <div class="dashbord_left_ir" id="mobile_menu_dv">
                  <div class="dashbord_left_top">
                     <div class="media">
                        <em><img src="images/pro_pick.png" alt=""/></em>
                        <div class="media-body">
                           <h4>John Graham</h4>
                           <p>dummy-email12@gmail.com</p>
                        </div>
                     </div>
                  </div>
                  <div class="dash_pro_link">
                    <ul>
                        <li><a href="dashboard.html"><img src="images/das_icon1.png" alt=""/><span>Dashboard </span></a></li>
                        <li><a href="order-history.html"><img src="images/das_icon2.png" alt=""/><span>Order History</span></a></li>
                        <li><a href="address-book.html" class="actv"><img src="images/das_icon3.png" alt=""/><span>Address Book</span></a></li>
                        <li><a href="payment-history.html"><img src="images/das_icon4.png" alt=""/><span>Payment History</span></a></li>
                        <li><a href="my-favorites.html"><img src="images/das_icon5.png" alt=""/><span>My Favorites</span></a></li>
                        <li><a href="index.html"><img src="images/das_icon6.png" alt=""/><span>Log Out</span></a></li>
                    </ul>
                  </div>
               </div>
            </div>
            <div class="dashbord_right">
               <div class="dashbord_right_ir">
                  <h1>Add Address</h1>
                  <div class="dashbord_frm">
                  <form>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="tit_bx">
                              <h4>Basic information</h4>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Address Title </label>
                              <input type="text" placeholder="Enter here" value="Home" class="newDe_input_acv"/>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>First Name *</label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Last Name *</label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="tit_bx">
                              <h4>Address Details</h4>
                           </div>
                        </div>
                        <div class="col-lg-8 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Full Address *  </label>
                              <input type="text" placeholder="Enter your full address here" value=""/>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Country *</label>
                              <select>
                                 <option>Enter here</option>
                                 <option>India</option>
                                 <option>USA</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>State * </label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                         <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Town/City *</label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                         <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Postcode *</label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="tit_bx">
                              <h4>Contact Information</h4>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Phone Number </label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Mobile Number *</label>
                              <input type="text" placeholder="Enter here"/>
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="dashbord_input">
                              <label>Email Addres *</label>
                              <input type="email" placeholder="Enter here"/>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12"> 
                            <div class="checkout_ship">
                              <div class="checkout_add">
                                 <div class="input_fields">
                                 <label class="list_checkBox">
                                    <p class="new_label">  Set as default Shipping address</p>
                                    <input type="checkbox" name="text" id="differentaddress" checked=""/>
                                    <span class="contect_checkmark list_checkmark new_achec" for="differentaddress"></span>
                                 </label>
                                 </div>                                        
                                 <div class="different_address" style={{display: "none"}}>
                                   <div class="row">
                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>Address Title </label>
                                             <input type="text" placeholder="Enter here" value="Home"/>
                                          </div>
                                       </div>
                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>First Name *</label>
                                             <input type="text" placeholder="Enter here"/>
                                          </div>
                                       </div>
                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>Last Name *</label>
                                             <input type="text" placeholder="Enter here"/>
                                          </div>
                                       </div>
                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>Full Address *  </label>
                                             <input type="text" placeholder="Enter your full address here" value=""/>
                                          </div>
                                       </div>
                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>Country *</label>
                                             <select>
                                                <option>Enter here</option>
                                                <option>India</option>
                                                <option>USA</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>State * </label>
                                             <input type="text" placeholder="Enter here"/>
                                          </div>
                                       </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>Town/City *</label>
                                             <input type="text" placeholder="Enter here"/>
                                          </div>
                                       </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                          <div class="dashbord_input">
                                             <label>Postcode *</label>
                                             <input type="text" placeholder="Enter here"/>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="save_adres_bx">
                        <button class="pag_arwbtn das_btn">Save Addres</button>
                     </div>
                  </form>   
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</Layout>
  );
}

export default AddAddress;
