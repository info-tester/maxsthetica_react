import React from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';
import {ErrorMessage, Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import cart1 from '../../assets/images/cart1.png'
import cart2 from '../../assets/images/cart2.png'
import cart3 from '../../assets/images/cart3.png'
import uploadnews from '../../assets/images/uploadnews.png'
import uploadnews1 from '../../assets/images/uploadnews1.png'




const Checkout =(props)=>{
 
  return (
   <Layout>
    <div className="mar_top"></div>
      <div className="main_wrapper">
         <div className="container">
            <div className="row">
               <div className="col-lg-12">
                  <div className="br_camp mt-3">
                     <ul>
                        <li><a href="#url">Home</a></li>
                        <li><a href="#url">medicine</a></li>
                        <li><a href="#url">Budetrol 200</a></li>
                        <li><a href="#url"> Shopping Cart</a></li>
                        <li><span> Checkout</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <section className="cart_sec">
            <div className="container">
               <div className="row">
                  <div className="col-lg-8">
                     <div className="cart_topics">
                        <h2>3 Items In Cart</h2>
                        <div className="cart_items">
                           <div className="cart_left">
                              <span>
                              <img src={cart1}/>
                              </span>
                              <div className="cart_info">
                                 <h3>Budetrol 200 Rotacap 30'S <span>Rx Requited</span></h3>
                                 <em>Mfr :  Sun pharma Pvt Ltd</em>
                                 <ul>
                                    <li>250 ml</li>
                                    <li className="pp_list"><del>£400</del>  £300</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div className="cart_items">
                           <div className="cart_left">
                              <span>
                              <img src={cart2}/>
                              </span>
                              <div className="cart_info">
                                 <h3>Calpol 650 mg</h3>
                                 <em>Mfr :  Mfr: Glenmark Pharmaceuticals Ltd</em>
                                 <ul>
                                    <li>500 ml</li>
                                    <li className="pp_list"><del>£20</del>  £10</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div className="cart_items">
                           <div className="cart_left">
                              <span>
                              <img src={cart3}/>
                              </span>
                              <div className="cart_info">
                                 <h3>Montek lc <span>Rx Requited</span></h3>
                                 <em>Mfr :  Mfr: Glenmark Pharmaceuticals Ltd</em>
                                 <ul>
                                    <li>250 ml</li>
                                    <li className="pp_list"><del>£200</del>  £150</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        
                     </div>


                  </div>

                  <div className="col-lg-4">
                     <div className="summary summary-cart new-amatr">
                        <h3 className="summary-title">Payment Details</h3>
                        <div className="sub_summary">
                           <div className="cart_summary_info">
                              <p>Subtotal</p>
                              <p className="text-right">£620.00</p>
                           </div>
                           <div className="cart_summary_info">
                              <p>Shipping charges</p>
                              <p className="text-right">£10.00</p>
                           </div>
                           <div className="cart_summary_info">
                              <p>Discount</p>
                              <p className="text-right">£170.00</p>
                           </div>
                        </div>
                        <div className="total_cart_div">
                           <h1>Total payable amount : £470.00</h1>
                        </div>
                     </div>
                  </div>

                  <div className="col-lg-8">
                     

                     <div className="cart_topics">
                        <div className="checkout_add">
                           <h3>Shipping Information</h3>

                           <div className="input-section-check">
                              <div className="form-group fr_rdoo">
                                 <label className="radio">
                                    <input id="radio1" type="radio" name="shiiping" checked="checked"/>
                                    <span className="outer"><span className="inner"></span></span>Create New Address
                                 </label>
                                 <label className="radio">
                                    <input id="radio1" type="radio" name="shiiping"/>
                                    <span className="outer"><span className="inner"></span></span>Use Saved Address
                                 </label>
                                 <div className="clearfix"></div>
                              </div>
                           </div>

                            <form autocomplete="off">
                              <div className="input-from">
                                 <div className="row">
                                   
                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">First Name *</label>
                                          <input type="text" name="" placeholder="Enter First Name" value="Rabin" required=""/>
                                      </div>
                                   </div>
                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Last Name * </label>
                                          <input type="text" name="" placeholder="Enter your last name" required=""/>
                                      </div>
                                   </div>
                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Email Address* </label>
                                          <input type="text" name="" placeholder="Enter Email Address" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Phone Number *  </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Street Address * </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Address Line 2</label>
                                          <input type="text" name="" placeholder="Enter here"/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Town/City * </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Postcode * </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>
                                </div>
                              </div>
                           </form>

                        </div>
                     </div>

                     <div className="cart_topics">
                        <div className="checkout_add">
                           <h3>Billing Address</h3>
                           <div className="input_fields">
                           <label className="list_checkBox">
                              <p className="new_label">  Billing Address same as Shipping </p>
                              <input type="checkbox" name="text" id="differentaddress" checked=""/>
                              <span className="contect_checkmark list_checkmark new_achec" htmlFor="differentaddress"></span>
                           </label>
                           </div>
                                     
                           <div className="different_address" style={{display: "none"}}>
                            <form autocomplete="off">
                              <div className="input-from">
                                 <div className="row">
                                   
                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">First Name *</label>
                                          <input type="text" name="" placeholder="Enter First Name" value="Rabin" required=""/>
                                      </div>
                                   </div>
                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">
                                          <label htmlFor="name">Last Name * </label>
                                          <input type="text" name="" placeholder="Enter your last name" required=""/>
                                      </div>
                                   </div>
                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Email Address* </label>
                                          <input type="text" name="" placeholder="Enter Email Address" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Phone Number *  </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Street Address * </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Address Line 2</label>
                                          <input type="text" name="" placeholder="Enter here"/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Town/City * </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>

                                   <div className="col-lg-6 col-sm-6">
                                      <div className="input_fields">                                   
                                          <label htmlFor="name">Postcode * </label>
                                          <input type="text" name="" placeholder="Enter here" required=""/>
                                       </div>
                                   </div>
                                </div>
                              </div>
                           </form>
                        </div>
                        </div>
                     </div>


                     <div className="cart_topics">
                        <div className="checkout_add">
                           <h3>Upload Prescription * </h3>
                                     
                           <div className="uplodimgfilnew">
                                             <input type="file" name="file-1[]" id="file-1" className="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple=""/>
                                             <label htmlFor="file-1">
                                                <img src={uploadnews} alt="" className="hoven"/>
                                                <img src={uploadnews1} alt="" className="hoveb"/>
                                                <p>Drop your prescription here or click here to upload </p>
                                                <span>You can upload up to 4 pictures</span>
                                             </label>
                                          </div>
                        </div>
                     </div>

                     <div className="row">
                                 <div className="col-12">
                                    <div className="text-center">
                                    <button className="subm_btns">Confirm and Place Order</button>
                                    </div>
                                 </div>
                                </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
</Layout>

  );
}

export default Checkout;
