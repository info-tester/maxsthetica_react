import React from 'react';
import Layout from '../layout/Layout'
import { Link } from "react-router-dom";
import { useFormik } from 'formik';


import cart1 from '../../assets/images/cart1.png';
import cart2 from '../../assets/images/cart2.png';
import cart3 from '../../assets/images/cart3.png';
import del from '../../assets/images/del.png';
import delh from '../../assets/images/delh.png';


const ShoppingCart =(props)=>{
 
  return (
   <Layout>
	   <div className="mar_top"></div>
      <div className="main_wrapper">
         <div className="container">
            <div className="row">
               <div className="col-lg-12">
                  <div className="br_camp mt-3">
                     <ul>
                        <li><Link to="/">Home</Link></li>
                        <li><a href="#url">medicine</a></li>
                        <li><a href="#url">Budetrol 200</a></li>
                        <li><span> Shopping Cart</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <section className="cart_sec">
            <div className="container">
               <div className="row">
                  <div className="col-lg-8">
                     <div className="cart_topics">
                        <h2>2 Items In Cart</h2>
                        <div className="cart_items">
                           <div className="cart_left">
                              <span>
                              <img src={cart1}/>
                              </span>
                              <div className="cart_info">
                                 <h3>Budetrol 200 Rotacap 30'S <span>Rx Requited</span></h3>
                                 <em>Mfr :  Sun pharma Pvt Ltd</em>
                                 <ul>
                                    <li>250 ml</li>
                                    <li className="pp_list"><del>£400</del>  £300</li>
                                 </ul>
                              </div>
                           </div>
                           <div className="cart_right">
                              <a href="#"><img src={del} className="hobern"/>
                              <img src={delh} className="hoberb"/></a>
                              <div className="cart_quan">
                                 <div className="product-quantity">
                                    <div className="quantity-selectors">
                                       <button type="button" className="decrement-quantity" aria-label="Subtract one" data-direction="-1" disabled="disabled"><span>&#8722;</span></button>
                                       <input data-min="1" data-max="0" type="text" name="quantity" value="1" readonly="true"/>
                                       <button type="button" className="increment-quantity" aria-label="Add one" data-direction="1"><span>&#43;</span></button>
                                       <div className="clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div className="cart_items">
                           <div className="cart_left">
                              <span>
                              <img src={cart2}/>
                              </span>
                              <div className="cart_info">
                                 <h3>Calpol 650 mg</h3>
                                 <em>Mfr :  Mfr: Glenmark Pharmaceuticals Ltd</em>
                                 <ul>
                                    <li>500 ml</li>
                                    <li className="pp_list"><del>£20</del>  £10</li>
                                 </ul>
                              </div>
                           </div>
                           <div className="cart_right">
                              <a href="#"><img src={del} className="hobern"/>
                              <img src={delh} className="hoberb"/></a>
                              <div className="cart_quan">
                                 <div className="product-quantity">
                                    <div className="quantity-selectors">
                                       <button type="button" className="decrement-quantity1" aria-label="Subtract two" data-direction="-1" disabled="disabled"><span>&#8722;</span></button>
                                       <input data-min="1" data-max="0" type="text" name="quantity1" value="1" readonly="true"/>
                                       <button type="button" className="increment-quantity1" aria-label="Add two" data-direction="1"><span>&#43;</span></button>
                                       <div className="clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div className="cart_items">
                           <div className="cart_left">
                              <span>
                              <img src={cart3}/>
                              </span>
                              <div className="cart_info">
                                 <h3>Montek lc <span>Rx Requited</span></h3>
                                 <em>Mfr :  Mfr: Glenmark Pharmaceuticals Ltd</em>
                                 <ul>
                                    <li>250 ml</li>
                                    <li className="pp_list"><del>£200</del>  £150</li>
                                 </ul>
                              </div>
                           </div>
                           <div className="cart_right">
                              <a href="#"><img src={del} className="hobern"/>
                              <img src={delh} className="hoberb"/></a>
                              <div className="cart_quan">
                                 <div className="product-quantity">
                                    <div className="quantity-selectors">
                                       <button type="button" className="decrement-quantity2" aria-label="Subtract three" data-direction="-1" disabled="disabled"><span>&#8722;</span></button>
                                       <input data-min="1" data-max="0" type="text" name="quantity2" value="1" readonly="true"/>
                                       <button type="button" className="increment-quantity2" aria-label="Add one" data-direction="1"><span>&#43;</span></button>
                                       <div className="clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                  </div>
                  <div className="col-lg-4">
                     <div className="summary summary-cart">
                        <h3 className="summary-title">Payment Details</h3>
                        <div className="sub_summary">
                           <div className="cart_summary_info">
                              <p>Subtotal</p>
                              <p className="text-right">£620.00</p>
                           </div>
                           <div className="cart_summary_info">
                              <p>Shipping charges</p>
                              <p className="text-right">£10.00</p>
                           </div>
                           <div className="cart_summary_info">
                              <p>Discount</p>
                              <p className="text-right">£170.00</p>
                           </div>
                        </div>
                        <div className="total_cart_div">
                           <h1>Total payable amount : £470.00</h1>
                        </div>
                        <div className="cart_btns">
                           <Link to="/checkout">Continue to Checkout</Link>
                           <Link to="/search_products" className=" border_btns">Continue Shopping</Link>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
</Layout>

  );
}

export default ShoppingCart;
