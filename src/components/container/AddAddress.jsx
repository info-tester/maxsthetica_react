import React, { Component, useState } from "react";
import Layout from "../layout/Layout2";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import { ErrorMessage, Formik, Form, Field } from "formik";
import * as Yup from "yup";
import FormikErrorFocus from "formik-error-focus";

import pro_pick from "../../assets/images/pro_pick.png";
import das_icon1 from "../../assets/images/das_icon1.png";
import das_icon2 from "../../assets/images/das_icon2.png";
import das_icon3 from "../../assets/images/das_icon3.png";
import das_icon4 from "../../assets/images/das_icon4.png";
import das_icon5 from "../../assets/images/das_icon5.png";
import das_icon6 from "../../assets/images/das_icon6.png";

const phoneRegExp = /^[0-9\b]+$/;

const AddSchema = Yup.object().shape({
  title: Yup.string().required("Please enter address title"),
  title2:Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter address title"),
  }) ,
  email: Yup.string().required("Please enter email ").email(),
  fname: Yup.string().required("Please enter first name"),
  lname: Yup.string().required("Please enter last name"),
  fname2:Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter first name"),
  }) ,
  lname2:Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter last name"),
  }) ,
  contact_phone: Yup.string()
    .matches(phoneRegExp, "Phone number is not valid")
    .required("Please enter mobile number")
    .min(10, "Phone number must be 10 digits")
    .max(10, "Phone number must be 10 digits"),
  full_address: Yup.string().required("Please enter full address"),
  country: Yup.string().required("Please select country"),
  state: Yup.string().required("Please enter state"),
  city: Yup.string().required("Please enter city"),
  postcode: Yup.string().required("Please enter postcode"),

checkbox: Yup.boolean(),

  full_address2:Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter full address"),

  }) ,
  country2:Yup.string().when("checkbox",{
    is: false,
    then:  Yup.string().required("Please select country"),
  }),
  state2: Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter state"),
  }) ,
  city2: Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter town/city"),
  }) ,
  postcode2: Yup.string().when("checkbox",{
    is: false,
    then: Yup.string().required("Please enter postcode"),
  }) ,


});

class AddAddress extends Component {
  render() {
    return (
      <Layout>
        <div className="mar_top"></div>

        <div className="dashbord_sec">
          <div className="container">
            <div className="dashbord_inr">
              <div className="dashbord_left">
                <div className="mobile_menu" id="mobile_menu">
                  {" "}
                  <i className="fa fa-bars"></i>
                  <span>Show Menu</span>
                </div>
                <div className="dashbord_left_ir" id="mobile_menu_dv">
                  <div className="dashbord_left_top">
                    <div className="media">
                      <em>
                        <img src={pro_pick} alt="" />
                      </em>
                      <div className="media-body">
                        <h4>John Graham</h4>
                        <p>dummy-email12@gmail.com</p>
                      </div>
                    </div>
                  </div>
                  <div className="dash_pro_link">
                    <ul>
                      <li>
                        <Link to="/dashboard">
                          <img src={das_icon1} alt="" />
                          <span>Dashboard </span>
                        </Link>
                      </li>
                      <li>
                        <a href="">
                          <img src={das_icon2} alt="" />
                          <span>Order History</span>
                        </a>
                      </li>
                      <li>
                        <Link to="/address-book" className="actv">
                          <img src={das_icon3} alt="" />
                          <span>Address Book</span>
                        </Link>
                      </li>
                      <li>
                        <a href="">
                          <img src={das_icon4} alt="" />
                          <span>Payment History</span>
                        </a>
                      </li>
                      <li>
                        <a href="">
                          <img src={das_icon5} alt="" />
                          <span>My Favorites</span>
                        </a>
                      </li>
                      <li>
                        <Link to="/">
                          <img src={das_icon6} alt="" />
                          <span>Log Out</span>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="dashbord_right">
                <div className="dashbord_right_ir">
                  <h1>Add Address</h1>
                  <Formik
                    initialValues={{
                      title: "",
                      title2: "",
                      email: "",

                      fname: "",
                      lname: "",
                      fname2: "",
                      lname2: "",
                      phone: "",
                      contact_phone: "",

                      full_address: "",
                      country: "",
                      state: "",
                      city: "",
                      postcode: "",
                      full_address2: "",
                      country2: "",
                      state2: "",
                      city2: "",
                      postcode2: "",

                      checkbox: true,
                    }}
                    validationSchema={AddSchema}
                    onSubmit={(values) => {
                      console.log("==========", values);
                    }}
                  >
                    {({ errors, touched, values, setFieldValue }) => (
                      <Form>
                        <div className="dashbord_frm">
                          <div className="row">
                            <div className="col-sm-12">
                              <div className="tit_bx">
                                <h4>Basic information</h4>
                              </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Address Title </label>
                                <Field
                                  type="text"
                                  name="title"
                                  placeholder="Enter here"
                                  className="newDe_input_acv"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="title" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>First Name *</label>
                                <Field
                                  type="text"
                                  name="fname"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="fname" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Last Name *</label>
                                <Field
                                  type="text"
                                  name="lname"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="lname" />
                              </span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12">
                              <div className="tit_bx">
                                <h4>Address Details</h4>
                              </div>
                            </div>
                            <div className="col-lg-8 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Full Address * </label>
                                <Field
                                  type="text"
                                  name="full_address"
                                  placeholder="Enter your full address here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="full_address" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Country *</label>
                                <Field as="select" name="country" >
                                  <option value="">Enter here</option>
                                  <option value="India">India</option>
                                  <option value="USA">USA</option>
                                </Field>
                              </div>
                              <span className="error">
                                <ErrorMessage name="country" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>State * </label>
                                <Field
                                  type="text"
                                  name="state"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="state" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Town/City *</label>
                                <Field
                                  type="text"
                                  name="city"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="city" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Postcode *</label>
                                <Field
                                  type="text"
                                  name="postcode"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="postcode" />
                              </span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12">
                              <div className="tit_bx">
                                <h4>Contact Information</h4>
                              </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Phone Number </label>
                                <Field
                                  type="text"
                                  name="phone"
                                  placeholder="Enter here"
                                />
                              </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Mobile Number *</label>
                                <Field
                                  type="text"
                                  name="contact_phone"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="contact_phone" />
                              </span>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-6">
                              <div className="dashbord_input">
                                <label>Email Addres *</label>
                                <Field
                                  type="email"
                                  name="email"
                                  placeholder="Enter here"
                                />
                              </div>
                              <span className="error">
                                <ErrorMessage name="email" />
                              </span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-12">
                              <div className="checkout_ship">
                                <div className="checkout_add">
                                  <div className="input_fields">
                                    <label className="list_checkBox">
                                      <p className="new_label">
                                        {" "}
                                        Set as default Shipping address
                                      </p>
                                      <Field
                                        type="checkbox"
                                        name="checkbox"
                                        id="differentaddress"
                                        onChange={(event) => {
                                          setFieldValue(
                                            "checkbox",
                                            !values.checkbox
                                          );
                                        }}
                                      />
                                      <span
                                        className="contect_checkmark list_checkmark new_achec"
                                        htmlFor="differentaddress"
                                      ></span>
                                    </label>
                                  </div>
                                  {!values.checkbox ? (
                                    <div className="different_address">
                                      <div className="row">
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>Address Title </label>
                                            <Field
                                              type="text"
                                              name="title2"
                                              placeholder="Enter here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="title2" />
                                          </span>
                                        </div>
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>First Name *</label>
                                            <Field
                                              type="text"
                                              name="fname2"
                                              placeholder="Enter here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="fname2" />
                                          </span>
                                        </div>
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>Last Name *</label>
                                            <Field
                                              type="text"
                                              name="lname2"
                                              placeholder="Enter here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="lname2" />
                                          </span>
                                        </div>
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>Full Address * </label>
                                            <Field
                                              type="text"
                                              name="full_address2"
                                              placeholder="Enter your full address here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="full_address2" />
                                          </span>
                                        </div>
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>Country *</label>
                                            <Field as="select" name="country2">
                                              <option value="">
                                                Enter here
                                              </option>
                                              <option value="India">
                                                India
                                              </option>
                                              <option value="USA">
                                                USA
                                              </option>
                                            </Field>
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="country2" />
                                          </span>
                                        </div>

                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>State * </label>
                                            <Field
                                              type="text"
                                              name="state2"
                                              placeholder="Enter here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="state2" />
                                          </span>
                                        </div>
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>Town/City *</label>
                                            <Field
                                              type="text"
                                              name="city2"
                                              placeholder="Enter here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="city2" />
                                          </span>
                                        </div>
                                        <div className="col-lg-4 col-md-6 col-sm-6">
                                          <div className="dashbord_input">
                                            <label>Postcode *</label>
                                            <Field
                                              type="text"
                                              name="postcode2"
                                              placeholder="Enter here"
                                            />
                                          </div>
                                          <span className="error">
                                            <ErrorMessage name="postcode2" />
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  ) : null}
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="save_adres_bx">
                            <button
                              type="submit"
                              className="pag_arwbtn das_btn"
                            >
                              Save Addres
                            </button>
                          </div>
                        </div>
                        <FormikErrorFocus
                          offset={0}
                          align={"top"}
                          focusDelay={200}
                          ease={"linear"}
                          duration={1000}
                        />
                      </Form>
                    )}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}
export default AddAddress;
