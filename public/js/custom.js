	$('.carousel').carousel({
	  interval: 5000,
   	  pause: "false"
	})

$(".navbar-toggler").click(function(){
  $(".navbar-toggler").toggleClass("showtoggle");
}); 


// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
// $('#return-to-top').click(function() {      // When arrow is clicked
//     $('body,html').animate({
//         scrollTop : 0                       // Scroll to top of body
//     }, 500);
// });
$(document).on('click','#return-to-top',function() {      // When arrow is clicked
  $('body,html').animate({
      scrollTop : 0                       // Scroll to top of body
  }, 500);
});

$(window).scroll(function() { 

    var scroll = $(window).scrollTop();
    if (scroll >= 300) {

        $(".foot_arw").addClass("fixed");

    } else {

        $(".foot_arw").removeClass("fixed");

    }

});


 $(document).ready(function(){
    $(document).on('click',"#category_click",function(){
        $("#category_div").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#category_div').length
    && !$target.closest('#category_click').length
    && $('#category_div').is(":visible")) {
    $('#category_div').slideUp();
  }
  
});

 $(document).ready(function(){
    $(document).on("click","#brand_click",function(){
        $("#brand_div").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#brand_div').length
    && !$target.closest('#brand_click').length
    && $('#brand_div').is(":visible")) {
    $('#brand_div').slideUp();
  }
  
});

$('#moreless-button').click(function() {
  $('#moretext').slideToggle();
  if ($('#moreless-button').text() == "+ More") {
    $(this).text("- Less")
  } else {

    $(this).text("+ More")
  }
});

$('#moreless-button1').click(function() {
  $('#moretext1').slideToggle();
  if ($('#moreless-button1').text() == "+ More") {
    $(this).text("- Less")
  } else {

    $(this).text("+ More")
  }
});


$(document).ready(function(){
    $("#mobile-list").click(function(){
        $("#filter_bx").slideToggle();
    });
})


   $(".increment-quantity,.decrement-quantity").on("click", function(ev) {
      var currentQty = $('input[name="quantity"]').val();
      var qtyDirection = $(this).data("direction");
      var newQty = 0;
      if(qtyDirection == "1") {
         newQty = parseInt(currentQty) + 1;
      } else if(qtyDirection == "-1") {
         newQty = parseInt(currentQty) - 1;
      }
      // make decrement disabled at 1
      if(newQty == 1) {
         $(".decrement-quantity").attr("disabled", "disabled");
      }
      // remove disabled attribute on subtract
      if(newQty > 1) {
         $(".decrement-quantity").removeAttr("disabled");
      }
      if(newQty > 0) {
         newQty = newQty.toString();
         $('input[name="quantity"]').val(newQty);
      } else {
         $('input[name="quantity"]').val("1");
      }
   });
 

$(".increment-quantity1,.decrement-quantity1").on("click", function(ev) {
      var currentQty = $('input[name="quantity1"]').val();
      var qtyDirection = $(this).data("direction");
      var newQty = 0;
      if(qtyDirection == "1") {
         newQty = parseInt(currentQty) + 1;
      } else if(qtyDirection == "-1") {
         newQty = parseInt(currentQty) - 1;
      }
      // make decrement disabled at 1
      if(newQty == 1) {
         $(".decrement-quantity1").attr("disabled", "disabled");
      }
      // remove disabled attribute on subtract
      if(newQty > 1) {
         $(".decrement-quantity1").removeAttr("disabled");
      }
      if(newQty > 0) {
         newQty = newQty.toString();
         $('input[name="quantity1"]').val(newQty);
      } else {
         $('input[name="quantity1"]').val("1");
      }
   });


$(".increment-quantity2,.decrement-quantity2").on("click", function(ev) {
      var currentQty = $('input[name="quantity2"]').val();
      var qtyDirection = $(this).data("direction");
      var newQty = 0;
      if(qtyDirection == "1") {
         newQty = parseInt(currentQty) + 1;
      } else if(qtyDirection == "-1") {
         newQty = parseInt(currentQty) - 1;
      }
      // make decrement disabled at 1
      if(newQty == 1) {
         $(".decrement-quantity2").attr("disabled", "disabled");
      }
      // remove disabled attribute on subtract
      if(newQty > 1) {
         $(".decrement-quantity2").removeAttr("disabled");
      }
      if(newQty > 0) {
         newQty = newQty.toString();
         $('input[name="quantity2"]').val(newQty);
      } else {
         $('input[name="quantity2"]').val("1");
      }
   });


//   $(".toggle-password").click(function() {

//   $(this).toggleClass("icofont-eye-blocked");
//   var input = $($(this).attr("toggle"));
//   if (input.attr("type") == "password") {
//     input.attr("type", "text");
//   } else {
//     input.attr("type", "password");
//   }
// });

//    $(".toggle-passwordnew").click(function() {

//   $(this).toggleClass("icofont-eye-blocked");
//   var input = $($(this).attr("toggle"));
//   if (input.attr("type") == "password") {
//     input.attr("type", "text");
//   } else {
//     input.attr("type", "password");
//   }
// });



/*===================================*
  24. CHECKBOX CHECK THEN ADD CLASS JS
  *===================================*/
  $('.different_address').hide();
  
  $('#differentaddress:checkbox').on('change', function(){
    if(!$(this).is(":checked")) {
      $('.different_address').slideDown();
    } else {
      $('.different_address').slideUp();
    }
  });

  $(document).ready(function(){
    $(document).on("click","#profidrop",function(){
        $("#profidropdid").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid').length
    && !$target.closest('#profidrop').length
    && $('#profidropdid').is(":visible")) {
    $('#profidropdid').slideUp();
  }
  
});



$(document).ready(function(){
    $(document).on("click","#mobile_menu",function(){
        $("#mobile_menu_dv").slideToggle();
    });
});





 
$(document).ready(function() {
  $("#action4").click(function() {
    $("#show-action4").slideToggle();
  });
});
$(document).ready(function() {
  $("#action5").click(function() {
    $("#show-action5").slideToggle();
  });
});
$(document).ready(function() {
  $("#action6").click(function() {
    $("#show-action6").slideToggle();
  });
});
$(document).ready(function() {
  $("#action7").click(function() {
    $("#show-action7").slideToggle();
  });
});
$(document).ready(function() {
  $("#action8").click(function() {
    $("#show-action8").slideToggle();
  });
});
$(document).ready(function() {
  $("#action9").click(function() {
    $("#show-action9").slideToggle();
  });
});
$(document).ready(function() {
  $("#action10").click(function() {
    $("#show-action10").slideToggle();
  });
});
$(document).ready(function() {
  $("#action1").click(function() {
    $("#show-action1").slideToggle();
  });
});
$(document).ready(function() {
  $("#action2").click(function() {
    $("#show-action2").slideToggle();
  });
});
$(document).ready(function() {
  $("#action3").click(function() {
    $("#show-action3").slideToggle();
  });
});
$(document).ready(function() {
  $("#action11").click(function() {
    $("#show-action11").slideToggle();
  });
});
$(document).ready(function() {
  $("#action12").click(function() {
    $("#show-action12").slideToggle();
  });
});
$(document).ready(function() {
  $("#action13").click(function() {
    $("#show-action13").slideToggle();
  });
});
$(document).ready(function() {
  $("#action14").click(function() {
    $("#show-action14").slideToggle();
  });
});
$(document).ready(function() {
  $("#action15").click(function() {
    $("#show-action15").slideToggle();
  });
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action1').length && !$target.closest('#action1').length && $('#show-action1 ').is(":visible")) {
    $('#show-action1 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action2').length && !$target.closest('#action2').length && $('#show-action2 ').is(":visible")) {
    $('#show-action2 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action3').length && !$target.closest('#action3').length && $('#show-action3 ').is(":visible")) {
    $('#show-action3 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action4').length && !$target.closest('#action4').length && $('#show-action4 ').is(":visible")) {
    $('#show-action4').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action5').length && !$target.closest('#action5').length && $('#show-action5 ').is(":visible")) {
    $('#show-action5 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action6').length && !$target.closest('#action6').length && $('#show-action6 ').is(":visible")) {
    $('#show-action6 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action7').length && !$target.closest('#action7').length && $('#show-action7 ').is(":visible")) {
    $('#show-action7 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action8').length && !$target.closest('#action8').length && $('#show-action8 ').is(":visible")) {
    $('#show-action8 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action9').length && !$target.closest('#action9').length && $('#show-action9 ').is(":visible")) {
    $('#show-action9 ').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action10').length && !$target.closest('#action10').length && $('#show-action10').is(":visible")) {
    $('#show-action10').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action11').length && !$target.closest('#action11').length && $('#show-action11').is(":visible")) {
    $('#show-action11').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action12').length && !$target.closest('#action12').length && $('#show-action12').is(":visible")) {
    $('#show-action12').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action13').length && !$target.closest('#action13').length && $('#show-action13').is(":visible")) {
    $('#show-action13').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action14').length && !$target.closest('#action14').length && $('#show-action14').is(":visible")) {
    $('#show-action14').slideUp();
  }
});
$(document).on('click', function() {
  var $target = $(event.target);
  if(!$target.closest('#show-action15').length && !$target.closest('#action15').length && $('#show-action15').is(":visible")) {
    $('#show-action15').slideUp();
  }
});


  $(document).ready(function(){
    $("#profidrop1").click(function(){
        $("#profidropdid1").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid1').length
    && !$target.closest('#profidrop1').length
    && $('#profidropdid1').is(":visible")) {
    $('#profidropdid1').slideUp();
  }
  
});

  $(document).ready(function(){
    $("#profidrop2").click(function(){
        $("#profidropdid2").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid2').length
    && !$target.closest('#profidrop2').length
    && $('#profidropdid2').is(":visible")) {
    $('#profidropdid2').slideUp();
  }
  
});

  $(document).ready(function(){
    $("#profidrop3").click(function(){
        $("#profidropdid3").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid3').length
    && !$target.closest('#profidrop3').length
    && $('#profidropdid3').is(":visible")) {
    $('#profidropdid3').slideUp();
  }
  
});

  $(document).ready(function(){
    $("#profidrop4").click(function(){
        $("#profidropdid4").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid4').length
    && !$target.closest('#profidrop4').length
    && $('#profidropdid4').is(":visible")) {
    $('#profidropdid4').slideUp();
  }
  
});

  $(document).ready(function(){
    $("#profidrop5").click(function(){
        $("#profidropdid5").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid5').length
    && !$target.closest('#profidrop5').length
    && $('#profidropdid5').is(":visible")) {
    $('#profidropdid5').slideUp();
  }
  
});

  $(document).ready(function(){
    $("#profidrop6").click(function(){
        $("#profidropdid6").slideToggle();
    });
});
$(document).on('click', function () {
  var $target = $(event.target);
  if (!$target.closest('#profidropdid6').length
    && !$target.closest('#profidrop6').length
    && $('#profidropdid6').is(":visible")) {
    $('#profidropdid6').slideUp();
  }
  
});







$(document).ready(function(){
  $(document).on('click','#sign_btns',function() { 
      $("#sign_btn_bx").slideToggle();
  });
});
$(document).on('click', function () {
var $target = $(event.target);
if (!$target.closest('#sign_btn_bx').length
  && !$target.closest('#sign_btns').length
  && $('#sign_btn_bx').is(":visible")) {
  $('#sign_btn_bx').slideUp();
}

});