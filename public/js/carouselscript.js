
$(document).ready(function() {
    var owl = $('.popular_products_slid .owl-carousel');
    owl.owlCarousel({
      margin: 0,
      nav: true,
      autoplay: true,
      loop: true,
      responsive: {
        0: {
          items: 1
        },
        400: {
          items: 2
        },
        768: {
          items: 4
        },
        991: {
          items: 5
        },
        1000: {
          items: 6
        }
      }
    })
  })

// $(document).ready(function() {
//     var owl = $('.rel_products_slid .owl-carousel');
//     owl.owlCarousel({
//       margin: 0,
//       nav: true,
//       autoplay: true,
//       loop: true,
//       responsive: {
//         0: {
//           items: 1
//         },
//         400: {
//           items: 2
//         },
//         768: {
//           items: 3
//         },
//         991: {
//           items: 4
//         },
//         1000: {
//           items: 5
//         }
//       }
//     })
//   })
$(document).ready(function() {
    var owl = $('.clients_say_slid .owl-carousel');
    owl.owlCarousel({
      margin: 16,
      nav: true,
      autoplay: true,
      loop: true,
      responsive: {
        0: {
          items: 1
        },
        768: {
          items: 2
        },
        991: {
          items: 3
        },
        1000: {
          items: 3
        }
      }
    })
  })

